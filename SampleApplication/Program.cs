﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QUT.Bio.BioPatML.Patterns;
using Bio;
using System.Data;
using Bio.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using QUT.Bio.BioPatML.Sequences.Additional;

namespace SampleApplication
{
    /// <summary>
    /// This console application is a simple implementation on how the BioPatML library
    /// be used in programming. The user is expected to parse sequence in GenBank file
    /// and pattern in XML file. The user may have alook the ID, Alphabet name and sequence
    /// as well as the pattern format in XML.
    /// This application then search either All of matches or the
    /// best match and show the result into the screen.
    /// 
    /// </summary>
    class Program
    {
        private static List<Match> _matchList;

        private static IList<ISequence> _sequences;

        private static Definition _pattern;

        /// <summary>
        /// Enter the application
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            InitiateMatchList();
            DisplayWelcome();
            
            while(_sequences == null || _pattern == null)
            {
                ShowGuidance_FirstEnter();
            
                string option = Console.ReadLine();
                
                if (option.ToUpper() == "Q")
                    return;
                
                Run(option );
            }


            ShowGuidance_Second();

            string matchOption = Console.ReadLine();

            Match(matchOption);

            Console.ReadKey(true);
        }

        /// <summary>
        /// 
        /// </summary>
        private static void InitiateMatchList()
        {
            _matchList = new List<Match>();
        }

        /// <summary>
        /// Execute either a class for match based the user choice
        /// either Match All or Match Best
        /// </summary>
        /// <param name="matchOption">the option being input from user
        /// either match_all or match_best
        /// </param>
        private static void Match(string matchOption)
        {
            if(matchOption.IndexOf("match_all") > -1)
            {
                MatchAll();
                return;
            }
            else if (matchOption.IndexOf("match_best") > -1)
            {
                MatchBest();
                return;
            }
            else
            {
                Console.WriteLine("Wrong command.... :(");
            }
            
        }

        /// <summary>
        /// Search the best match in the sequence using the choosen pattern. The sequence would be picked
        /// from the first or default sequence in the list
        /// </summary>
        private static void MatchBest()
        {
            try
            {
                ISequence seq = _sequences.FirstOrDefault();
                Match match = Searcher.SearchBestMatch(seq, 0, (int)seq.Count - 1, _pattern.Pattern);
                ShowMatch(0, match);
            }
            catch
            {
                Console.WriteLine("Error to catch the match");    
            }
        }

        /// <summary>
        /// Search all matches in the sequence using the choosen pattern. The sequence would be picked
        /// from the first or default sequence in the list
        /// </summary>
        private static void MatchAll()
        {
            try
            {
                ISequence seq = _sequences.FirstOrDefault();
                _matchList = Searcher.SearchMatch(seq, 0, (int)seq.Count - 1, _pattern.Pattern);
            }
            catch
            {
                Console.WriteLine("Error to match the sequence with the pattern");
                return;
            }

            Console.WriteLine("do you want to show the result? Press Y if yes");
            if (Console.ReadLine().ToUpper() == "Y")
            {
                for (int i = 0; i < _matchList.Count; i++) 
                {
                    ShowMatch(i, _matchList[i]);
                }
            }

            Console.WriteLine("/n/nNumber of matches between pattern and sequence is " + _matchList.Count);
        }

        /// <summary>
        /// Display match into screen
        /// </summary>
        /// <param name="no"></param>
        /// <param name="match"></param>
        private static void ShowMatch(int no, QUT.Bio.BioPatML.Patterns.Match match)
        {
            Console.WriteLine("Match no : " + no);
            Console.WriteLine("Letter : " + match.Letters());
            Console.WriteLine("Start : " + match.Start);
            Console.WriteLine("Length : " + match.Count);
            Console.WriteLine("Similarity : " + match.Similarity);
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ShowGuidance_Second()
        {
            Console.WriteLine("Guidance" +
                "/n/n1. Please type match_all to get all matches /n/n" +
                "/n/n1. Please type match_best to get the best match /n/n");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="option"></param>
        private static void Run(string option)
        {
            
            if (option.IndexOf("load_gbk") > -1)
            {
                int startIdx = option.IndexOf(":")+1;
                bool run = true;
                
                    ParseGbk(option.Substring(startIdx, option.Length - startIdx), ref run);
                    if (_sequences != null)
                        Console.WriteLine("\n\nSuccess to parse sequence\n\n");
                return;
            }

            else if (option.IndexOf("load_xml") > -1)
            {
                int startIdx = option.IndexOf(":") + 1;
                bool run = true;
                while (run)
                    ParseXml(option.Substring(startIdx, option.Length - startIdx), ref run);

                if (_pattern != null)
                    Console.WriteLine("\n\nSuccess to parse pattern\n\n");

                return;
            }

            else
            {
                Console.WriteLine("\n\nWrong command !!!\n\n");
            }

        }

        /// <summary>
        /// Try to parse the pattern in XML file into Definition
        /// </summary>
        /// <param name="filename">The full path filename</param>
        /// <param name="failed"></param>
        private static void ParseXml(string filename, ref bool failed)
        {
            XDocument doc;
            try
            {
                doc = XDocument.Load(filename);
                _pattern = DefinitionIO.Read(doc);
                failed = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("cannot load the pattern into Definition :" + ex.ToString());
                Console.WriteLine("Do you want to try again? Press Y if yes");
                if (Console.ReadLine().ToUpper() == "Y")

                    failed = false;
                else
                    failed = true;

                return;
            }

            Console.WriteLine("Do you want to see the XML file??? Press Y if yes");

            if (Console.ReadLine().ToUpper() == "Y")
            {
                Console.Write(doc.ToString());
            }

            failed = false ;
        }

        /// <summary>
        /// Try to parse the sequence in GenBank file into Sequence object in  .NET Bio
        /// </summary>
        /// <param name="filename">fullpath filename</param>
        /// <param name="failed"></param>
        private static void ParseGbk(string filename, ref bool failed)
        {
            try
            {
                var parser = SequenceParsers.FindParserByFileName(filename);
                if (parser == null)
                {
                    Console.WriteLine("No parser available for the file.\n");
                    failed = false;
                    return;
                }
                    
                _sequences = parser.Parse().ToList();
                parser.Close();
                
                Console.WriteLine("Do you want to display the sequece?");
                string choose = Console.ReadLine();

                if (choose.ToUpper() == "Y")
                    ShowSequence();

                return;
            }
            catch(Exception e) 
            {
                
                Console.WriteLine("Failed to parse Genbank file" + e.ToString());

                Console.WriteLine("Do you want to continue parsing GenBank File? Press Y if yes");
                string choose = Console.ReadLine();

                if (choose.ToUpper() == "Y")
                    failed = true;
                else
                    failed = true;
            }
        }

        /// <summary>
        /// Display sequence in screen
        /// </summary>
        private static void ShowSequence()
        {
            ISequence seq = _sequences.FirstOrDefault();
            Console.WriteLine(seq.Alphabet.Name);
            Console.WriteLine(seq.ID);
            Console.WriteLine(seq.ToString());
            
        }

        /// <summary>
        /// 
        /// </summary>
        private static void ShowGuidance_FirstEnter()
        {
            Console.WriteLine("Guidance \n" +
                              "1. To load the Gen Bank Files type load_gbk:<fullpath of gbk>  \n" +
                              "  ex : load_gbk:c:\\myGbk.gbk\n" +
                              "2. To load the pattern xml, type load_xml:<fullpath of xml>  \n" +
                              "  ex : load_gbk:c:\\myPattern.xml\n");
        }

        /// <summary>
        /// 
        /// </summary>
        private static void DisplayWelcome()
        {
            Console.WriteLine("                 Welcome to Sample Application of BioPatML\n" +
                              "         =======================================================\n");
        }
    }

    
}
