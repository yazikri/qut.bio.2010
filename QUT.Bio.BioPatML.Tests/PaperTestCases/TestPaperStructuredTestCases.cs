﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using DB = System.Diagnostics.Debug;using Microsoft.VisualStudio.TestTools.UnitTesting;
using QUT.Bio.BioPatML.Sequences;
using QUT.Bio.BioPatML.Sequences.List;
using QUT.Bio.BioPatML.Patterns;
using BioPatML.Test;
using QUT.Bio.BioPatML.Sequences.Additional;
using Bio;
using QUT.Bio.BioPatML.Readers;

/*****************| Queensland  University Of Technology |*******************
 *  Author                   : Samuel Toh (Email: yu.toh@connect.qut.edu.au) 
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 *                             
 * All these additional test scenarios were taken from The BioPatML paper.
 * 
 ***************************************************************************/
namespace TestBioPatML.PaperTestCases
{
    [TestClass]
    public class TestPaperStructuredTestCases
    {
        /// <summary>
        /// All these test files are taken from MBF test suite
        /// </summary>
        private int SearchPosition { get; set; } //The position we wan to start search with
        private SequencesList BioList;
        private Definition MyPatterns;
        private string BiopatMLFilePath = string.Empty;
        private const string _genBankDataPath = @"data\GenBank";
        private const string _singleDnaSeqGenBankFilename = @"data\GenBank\D12555.gbk";

        [TestInitialize]
        public void SetUp()
        {
            this.BiopatMLFilePath = string.Empty;
            this.BioList = null;
            this.MyPatterns = null;
            SearchPosition = 1;
        }

        

        /// <summary>
        /// Test combining a series of ordered pattern consisting of 2 motifs with a gap
        /// inbetween them.
        /// This test scenario uses a makeup sequence to save time.
        /// Expected result: 2 matches
        /// </summary>
        [TestMethod]
        public void TestStructuredPattern_SeriesBest()
        {
            BiopatMLFilePath = "BioPaperTestData/StructuredPattern/SeriesBest.xml";

            ISequence sigma70Promoter = new Sequence(Alphabets.DNA, "ttgagggggttaccatgatcggtattgtttaatattgacatttaagccgttaagctgaagtgataattaggc");

			MyPatterns = DefinitionIO.Read( Global.GetResourceReader(   BiopatMLFilePath ) );

			var Matches = Searcher.SearchMatch( sigma70Promoter, 0, (int)sigma70Promoter.Count-1, MyPatterns.Pattern );

            //There should be 1 matches
            Assert.AreEqual(2, Matches.Count);
            // Assert.AreEqual("canonical sigma70-promoter", Matches.Name);

            Match matched = (Match)Matches[0];

            //The overall matched
            Assert.AreEqual("ttgagggggttaccatgatcggtattgtttaat", matched.Letters());
            Assert.AreEqual(0.75, matched.Similarity);
            //Check sigma 35
            Assert.AreEqual("ttgagg", matched.SubMatches[0].Letters());
            //Check sigma 10
            Assert.AreEqual("tttaat", matched.SubMatches[2].Letters());

            matched = (Match)Matches[1];
            Assert.AreEqual("ttgacatttaagccgttaagctgaagtgataat", matched.Letters());
            Assert.AreEqual(0.91, matched.Similarity, 1e-2);
        }

  

        /// <summary>
        /// Test Iteration element. 
        /// Using 2 characters to try iteration of 3 cycle.
        /// </summary>
        [TestMethod]
        public void TestStructuredPattern_Iteration()
        {
            BiopatMLFilePath = "BioPaperTestData/StructuredPattern/Iteration.xml";

            ISequence seq = new Sequence(Alphabets.DNA, "TTGAGAGATTTGCGCATC");
			MyPatterns = DefinitionIO.Read( Global.GetResourceReader(   BiopatMLFilePath ) );

			var Matches = Searcher.SearchMatch(seq, 0, (int)seq.Count-1, MyPatterns.Pattern );

            //There should be 3 matches
            //In this scenario only GAGA or GAGAGA is acceptable.
            Assert.AreEqual(2, Matches.Count);

            Match matched = (Match)Matches[0];
            Assert.AreEqual("gagaga", matched.Letters());

            matched = (Match)Matches[1];
            Assert.AreEqual("gaga", matched.Letters());
        }
    }
}
