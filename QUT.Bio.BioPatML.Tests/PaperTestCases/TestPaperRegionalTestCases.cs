﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using DB = System.Diagnostics.Debug;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QUT.Bio.BioPatML.Sequences.List;
using QUT.Bio.BioPatML.Patterns;
using BioPatML.Test;
using QUT.Bio.BioPatML.Sequences.Additional;
using QUT.Bio.BioPatML.Readers;

/*****************| Queensland  University Of Technology |*******************
 *  Author                   : Samuel Toh (Email: yu.toh@connect.qut.edu.au) 
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 *                             
 * All these additional test scenarios were taken from The BioPatML paper.
 * 
 ***************************************************************************/
namespace TestBioPatML.PaperTestCases {
	[TestClass]
	public class TestPaperRegionalTestCases {
		private int SearchPosition { get; set; } //The position we wan to start search with
		private SequencesList BioList;
		private Definition MyPatterns;
		private string BiopatMLFilePath = string.Empty;
		private const string _genBankDataPath = @"data\GenBank";
		private const string _singleProteinSeqGenBankFilename = @"data\GenBank\bioperlwiki.gbk";


		[TestInitialize]
		public void SetUp () {
			this.BiopatMLFilePath = string.Empty;
			this.BioList = null;
			this.MyPatterns = null;
			SearchPosition = 0;
		}

		/// <summary>
		/// Test "Any" element pattern.
		/// </summary>
		[TestMethod]
		public void TestRegionalPattern_Any () {
			BiopatMLFilePath = "BioPaperTestData/RegionalPattern/RegionalAny.xml";

			BioPatMBF_Reader gbReader = new BioPatMBF_Reader();

			BioList = gbReader.Read( Global.GetResourceReader( _singleProteinSeqGenBankFilename ) );

			MyPatterns = DefinitionIO.Read( Global.GetResourceReader( BiopatMLFilePath ) );

			var Matches = Searcher.SearchMatch( BioList[0], SearchPosition, (int)BioList[0].Count-1, MyPatterns.Pattern );

			//Total matches according to old jacobi is 309
			Assert.AreEqual( 309, Matches.Count );
			//Checks the first match 
			Assert.AreEqual( 0, Matches[0].Start );
			Assert.AreEqual( 5, Matches[0].End );
			//Checks if the last Match is in correect start and end pos
			Assert.AreEqual( 103, Matches[308].Start );
			Assert.AreEqual( 108, Matches[308].End );
		}

		/// <summary>
		/// Test the Gap pattern.
		/// 
		/// </summary>
		[TestMethod]
		public void TestRegionalPattern_Gap () {
			BiopatMLFilePath = "BioPaperTestData/RegionalPattern/RegionalGap.xml";

			BioPatMBF_Reader gbReader = new BioPatMBF_Reader();
			BioList = gbReader.Read( Global.GetResourceReader( _singleProteinSeqGenBankFilename ) );
			MyPatterns = DefinitionIO.Read( Global.GetResourceReader( BiopatMLFilePath ) );

			var Matches = Searcher.SearchMatch( BioList[0], SearchPosition, (int)BioList[0].Count-1, MyPatterns.Pattern );

			//Total matches according to old jacobi is 309
			Assert.AreEqual( 410, Matches.Count );
			//Checks the first match 
            Assert.AreEqual("MT", Matches[0].BaseSequence.GetSubSequence(Matches[0].Start, Matches[0].End - Matches[0].Start + 1).ToString().ToUpper());
			//Checks if the last Match is in correect start and end pos
            Assert.AreEqual("CE", Matches[409].BaseSequence.GetSubSequence(Matches[409].Start, Matches[0].End - Matches[0].Start + 1).ToString().ToUpper());
		}

		
	}
}
