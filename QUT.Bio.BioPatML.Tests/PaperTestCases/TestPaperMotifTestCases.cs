﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QUT.Bio.BioPatML.Patterns;
using QUT.Bio.BioPatML.Sequences;
using QUT.Bio.BioPatML.Sequences.List;
using DB = System.Diagnostics.Debug;
using BioPatML.Test;
using Bio;
using QUT.Bio.BioPatML.Sequences.Additional;
using QUT.Bio.BioPatML.Readers;

/*****************| Queensland  University Of Technology |*******************
 *  Author                   : Samuel Toh (Email: yu.toh@connect.qut.edu.au) 
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 *                             
 * All these additional test scenarios were taken from The BioPatML paper.
 * 
 ***************************************************************************/
namespace TestBioPatML.PaperTestCases {
	/// <summary>
	/// Common recursive pattern found in biology sequences.
	/// </summary>
	[TestClass]
	public class TestPaperMotifTestCases {
		/// <summary>
		/// All these test files are taken from MBF test suite
		/// </summary>
		// private ILog log;
		private int SearchPosition { get; set; } //The position we wan to start search with
		private SequencesList BioList;
		private Definition MyPatterns;
		private string BiopatMLFilePath = string.Empty;
		private const string _singleDnaSeqGenBankFilename = @"data\GenBank\D12555.gbk";
		private const string _sampleGenBankFile1 = @"data\GenBank\AE001582.gbk";
		private const string _sampleGenBankFile2 = @"data\GenBank\AF032047.gbk";

		//[TestFixtureSetUp]
		//public void FixtureSetUp()
		//{
		//    BasicConfigurator.Configure();
		//    log = LogManager.GetLogger(this.ToString());
		//}

		/// <summary>
		/// Setup the test items
		/// </summary>
		[TestInitialize]
		public void SetUp () {
			this.BiopatMLFilePath = string.Empty;
			this.BioList = null;
			this.MyPatterns = null;
			SearchPosition = 0;
		}

		/// <summary>
		/// Test element Motif.
		/// The famous pribnow-box will be used for testing in this case
		/// </summary>
		[TestMethod]
		public void TestMotifPattern_Motif () {
			BiopatMLFilePath = "BioPaperTestData/MotifPattern/Motif.xml";

			using ( BioPatMBF_Reader gbReader = new BioPatMBF_Reader() ) {
				BioList = gbReader.Read( Global.GetResourceReader( _singleDnaSeqGenBankFilename ) );
			}

			MyPatterns = DefinitionIO.Read( Global.GetResourceReader( BiopatMLFilePath ) );

			var Matches = Searcher.SearchMatch( BioList[0], SearchPosition, (int)(BioList[0].Count-1), MyPatterns.Pattern );

			//According to Jacobi library total matches should be 57 
			Assert.AreEqual( 57, Matches.Count );
			// Assert.AreEqual( "Pribnow-box", Matches.Name );
			//Perform some random checks from the 57 list

			Match matched = (Match) Matches[10]; //try get the 11th matched

			Assert.AreEqual( 0.66, matched.Similarity, 1e-2 );
			Assert.AreEqual( 6, matched.Count );
			Assert.AreEqual( "ttttat", matched.Letters() );

			//try the first match
			matched = (Match) Matches[0];
			Assert.AreEqual( DnaAlphabet.Instance, matched.Alphabet );
			Assert.AreEqual( 6, matched.Count );
			Assert.AreEqual( 0.5, matched.Similarity, 1e-2 );

			// Check the last match
			matched = (Match) Matches[56];
			Assert.AreEqual( 0.5, matched.Similarity, 1e-2 );
			Assert.AreEqual( "tttctt", matched.Letters() );

		}

		/// <summary>
		/// This test cases uses the C6 zinc finger in regular expression to search for matches
		/// within a sequence.
		/// </summary>
		[TestMethod]
		public void TestMotifPattern_RegularEx () {
            BiopatMLFilePath = "BioPaperTestData/MotifPattern/Regex.xml";

			ISequence dnaZincFinger = new Sequence( Alphabets.Protein, "ccccccaaaaaaccccccccactcttccccccccccctctctcccgcgctcacctggctcccccccccaatccgc" );

			MyPatterns = DefinitionIO.Read( Global.GetResourceReader( BiopatMLFilePath ) );

			var Matches = Searcher.SearchMatch( dnaZincFinger, SearchPosition, (int)dnaZincFinger.Count-1, MyPatterns.Pattern );

			//There should be 7 matches
			Assert.AreEqual( 7, Matches.Count );
			// Assert.AreEqual( "C6 zinc-finger", Matches.Name );

			Match matched = (Match) Matches[0];

			Assert.AreEqual( 2, matched.Start );
			Assert.AreEqual( 27, matched.Count );
			Assert.AreEqual( 28, matched.End );
            Assert.AreEqual("ccccaaaaaaccccccccactcttccc", matched.Letters());

			matched = (Match) Matches[1];

			Assert.AreEqual( 13, matched.Start );
			Assert.AreEqual( 28, matched.Count );
			Assert.AreEqual( 40, matched.End );
            Assert.AreEqual("cccccccactcttccccccccccctctc", matched.Letters());

			matched = (Match) Matches[2];
            Assert.AreEqual("ccccactcttccccccccccctctctcc", matched.Letters());

			matched = (Match) Matches[3];
            Assert.AreEqual("cttccccccccccctctctcccgcgctc", matched.Letters());
		}

		/// <summary>
		/// Test Prosite element.
		/// Similiar to regularexpression but with some differences in syntax 
		/// <para></para>
		/// Please see 
		/// http://au.expasy.org/txt/prosuser.txt.
		/// <para></para>
		/// For more prosite information
		/// </summary>
		[TestMethod]
		public void TestMotifPattern_Prosite () {
			BiopatMLFilePath = "BioPaperTestData/MotifPattern/Prosite.xml";

			ISequence dnaZincFinger = new Sequence( Alphabets.Protein, "ccccccaaaaaaccccccccactcttccccccccccctctctcccgcgctcacctggctcccccccccaatccgc" );

			MyPatterns = DefinitionIO.Read( Global.GetResourceReader( BiopatMLFilePath ) );

			var Matches = Searcher.SearchMatch( dnaZincFinger, SearchPosition, (int)dnaZincFinger.Count-1, MyPatterns.Pattern );

			//There should be 7 matches
			Assert.AreEqual( 7, Matches.Count );
			// Assert.AreEqual( "Leucine-zipper", Matches.Name );

			Match matched = (Match) Matches[4];

			Assert.AreEqual( 26, matched.Start );
			Assert.AreEqual( 53, matched.End );
			Assert.AreEqual( "CCCCCCCCCCCTCTCTCCCGCGCTCACC", matched.Letters().ToUpper() );

			matched = (Match) Matches[5];

			Assert.AreEqual( 33, matched.Start );
			Assert.AreEqual( 60, matched.End );
            Assert.AreEqual("CCCCTCTCTCCCGCGCTCACCTGGCTCC", matched.Letters().ToUpper());

			matched = (Match) Matches[6];
			Assert.AreEqual( 40, matched.Start );
			Assert.AreEqual( 67, matched.End );
            Assert.AreEqual("CTCCCGCGCTCACCTGGCTCCCCCCCCC", matched.Letters().ToUpper());
		}

		

		/// <summary>
		/// Test PWM element 
		/// </summary>
		[TestMethod]
		public void TestMotifPattern_PWM () {
			BiopatMLFilePath = "BioPaperTestData/MotifPattern/PWM.xml";

			using ( BioPatMBF_Reader gbReader = new BioPatMBF_Reader() ) {
				BioList = gbReader.Read( Global.GetResourceReader( _sampleGenBankFile2 ) );
			}

			MyPatterns = DefinitionIO.Read( Global.GetResourceReader( BiopatMLFilePath ) );
			var Matches = Searcher.SearchMatch( BioList[0], SearchPosition, (int)BioList[0].Count-1, MyPatterns.Pattern );

			Assert.AreEqual( 14, Matches.Count );
			//  Assert.AreEqual( "Pribnow-box", Matches.Name );

			Match matched = (Match) Matches[0];
			Assert.AreEqual( 16, matched.Start );
			Assert.AreEqual( 21, matched.End );
			Assert.AreEqual( 0.67, matched.Similarity, 1e-2 );
			Assert.AreEqual( "tctcct", matched.Letters() );

			matched = (Match) Matches[1];

			Assert.AreEqual( 24, matched.Start );
			Assert.AreEqual( 29, matched.End );
			Assert.AreEqual( 0.61, matched.Similarity, 1e-2 );
			Assert.AreEqual( "ttggct", matched.Letters() );

			matched = (Match) Matches[13];
			Assert.AreEqual( 267, matched.Start );
			Assert.AreEqual( 272, matched.End );
			Assert.AreEqual( 0.67, matched.Similarity, 1e-2 );
			Assert.AreEqual( "tgtgct", matched.Letters() );
		}
	}
}
