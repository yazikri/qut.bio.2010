﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using DB = System.Diagnostics.Debug;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QUT.Bio.BioPatML.Patterns;
using QUT.Bio.BioPatML.Common.XML;
using QUT.Bio.BioPatML.Sequences.Additional;
using BioPatML.Test;
using Bio;

/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/
namespace TestBioPatML.TestPatterns
{
    [TestClass]
    public class TestConstraint
    {

        /** test for match at sequence start */
        [TestMethod]
        public void TestMatchStart()
        {
            Sequence seq = new Sequence(Alphabets.DNA, "atgcatgc");
            Series series = new SeriesBest("test", 1.0);
            series.Add(new Constraint("test", "START", +1));

            Motif motif = new Motif("motif", Alphabets.DNA, "nnnn", 0.0);
            Match motifMatch = motif.Match(seq, 1);
            Assert.AreEqual("tgca", motifMatch.Letters());

            series.Add(motif);

            Match match = series.Match(seq, 1);
            Assert.AreEqual("tgca", match.Letters());
            match = series.Match(seq, 0);
            Assert.AreEqual(null, match);

            match = Searcher.SearchBestMatch(seq, 0, 0, series);
            Assert.AreEqual("tgca", match.Letters());
        }

        /** test for match at sequence end */
        [TestMethod]
        public void TestMatchEnd()
        {
            Sequence seq = new Sequence(Alphabets.DNA, "atgcatgc");
            Series series = new SeriesBest("test", 1.0);
            series.Add(new Motif("motif", Alphabets.DNA, "nnnn", 1.0));
            series.Add(new Constraint("test", "END", -1));

            Match match = series.Match(seq, 3);
            Assert.AreEqual("catg", match.Letters());

            match = series.Match(seq, 1);
            Assert.AreEqual(null, match);

            match = series.Match(seq, 5);
            Assert.AreEqual(null, match);

            match = Searcher.SearchBestMatch(seq,0, 0, series);
            Assert.AreEqual("catg", match.Letters());
        }

        /** test for match at sequence center */
        [TestMethod]
        public void TestMatchCenter()
        {
            Sequence seq = new Sequence(Alphabets.DNA, "atgcatg");
            Series series = new SeriesBest("test", 1.0);
            Constraint constraint = new Constraint("test", "CENTER", -1);
            series.Add(constraint);

            series.Add(new Motif("motif", Alphabets.DNA, "nnnn", 1.0));

            Match match = series.Match(seq, 2);
            Assert.AreEqual("gcat", match.Letters());
            match = series.Match(seq, 1);
            Assert.AreEqual(null, match);
            match = series.Match(seq, 4);
            Assert.AreEqual(null, match);

            Searcher.SearchBestMatch(seq, 0, 0, series);
            match = Searcher.SearchBestMatch(seq, 0, 0, series);
            Assert.AreEqual("gcat", match.Letters());
        }

        [TestMethod]
        public void TestRead()
        {
			Definition definition = DefinitionIO.Read( Global.GetResourceReader(  "BioPatMLXML/Constraint.xml" ) );
            Constraint pattern = (Constraint)((Series)definition.Pattern).Patterns[1];

            //Assert.AreEqual(1.0, pattern.Threshold); there is no threshold attr for constraint
            Assert.AreEqual("constraint", pattern.Name);
            Assert.AreEqual(0.9, pattern.Impact, 1e-3);
        }

        [TestMethod]
        public void TestToXml()
        {
			Definition definition = DefinitionIO.Read( Global.GetResourceReader(  "BioPatMLXML/Constraint.xml" ) );

			Assert.IsTrue( definition.ToXml().ToString().IndexOf( "name=\"auto-" ) < 0 );
			Definition def2 = DefinitionIO.Read( DefinitionIO.Write( definition ) );

			Constraint pattern = (Constraint) ( (Series) def2.Pattern ).Patterns[1];

            //Assert.AreEqual(1.0, pattern.Threshold); there is no threshold attr for constraint
            Assert.AreEqual("constraint", pattern.Name);
            Assert.AreEqual(0.9, pattern.Impact, 1e-3);
        }
  
    }
}
