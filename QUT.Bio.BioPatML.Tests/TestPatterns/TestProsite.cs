﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using DB = System.Diagnostics.Debug;using Microsoft.VisualStudio.TestTools.UnitTesting;
using QUT.Bio.BioPatML.Sequences.Additional;
using QUT.Bio.BioPatML.Patterns;
using QUT.Bio.BioPatML.Sequences.List;
using QUT.Bio.BioPatML.Common.XML;
using BioPatML.Test;
using Bio;

/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/
namespace TestBioPatML.TestPatterns
{
    [TestClass]
    public class TestProsite
    {

        [TestMethod]
        public void TestMatch()
        {
            IAlphabet alphabet = DnaAlphabet.Instance;
            ISequence seq = new Sequence(Alphabets.DNA, "acctccgg");
            Prosite prosite = new Prosite("c-t-c-x.", alphabet);
            Match match = prosite.Match(seq, 0);
            Assert.AreEqual(null, match);

            match = prosite.Match(seq, 2);
            Assert.AreEqual(2, match.Start);
            Assert.AreEqual(4, match.Count);
            //Assert.AreEqual(1, match.Strand);
            Assert.AreEqual(1.0, match.Similarity, 1e-2);


            prosite = new Prosite("a-c-c.", alphabet);
            match = prosite.Match(seq, 0);
            Assert.AreEqual(0, match.Start);

            prosite = new Prosite("<a-c-c.", alphabet);
            match = prosite.Match(seq, 2);
            Assert.AreEqual(null, match);

            prosite = new Prosite("c-g-g.", alphabet);
            match = prosite.Match(seq, 5);
            Assert.AreEqual(7, match.End);

            //prosite = new Prosite("c-y-y-c.", alphabet);//already commented out in previous project (LFY)
            //Assert.IsNotNull(prosite.Match(seq, 1));//already commented out in previous project (LFY)

            prosite = new Prosite("c-{d}-c.", alphabet);
            //Assert.IsNotNull(prosite.Match(seq, 1)); //<-- not sure why this pattern wont work

            prosite = new Prosite("c-x(0,2)-g.", alphabet);

            var matches = Searcher.SearchMatch(seq, 0 ,0 , prosite);
            
            Assert.AreEqual(2, matches.Count);
            Assert.AreEqual("ccgg", matches[0].Letters());
            Assert.AreEqual("cgg", matches[1].Letters());
            
        }

        [TestMethod]
        /** Tests the conversion of prosite patterns to regular expressions */
        public void TestConvertPattern()
        {
            IAlphabet alphabet = DnaAlphabet.Instance;
            Prosite prosite = new Prosite();
            Assert.AreEqual("actg", prosite.Convert("actg.", alphabet).ToString());
            Assert.AreEqual("[ac].[ga].{4}[^e[gat]]$", prosite.Convert("[ac]-x-r-x(4)-{ed}>.", alphabet).ToString());
            Assert.AreEqual("^a.[tg]{2}.{0,1}[ga]", prosite.Convert("<a-x-[tg](2)-x(0,1)-r.", alphabet).ToString());
        }

        [TestMethod]
        /** Tests the conversion of char to regular expressions */
        public void TestConvertChar()
        {
            IAlphabet alphabet = DnaAlphabet.Instance;
            Prosite prosite = new Prosite();
            Assert.AreEqual("a", prosite.Convert('a', alphabet));
            Assert.AreEqual("[ga]", prosite.Convert('r', alphabet));
            Assert.AreEqual("0", prosite.Convert('0', alphabet));
            Assert.AreEqual(",", prosite.Convert(',', alphabet));
        }

        [TestMethod]
        public void TestRead()
        {
			Definition definition = DefinitionIO.Read( Global.GetResourceReader(   "BioPatMLXML/Prosite.xml" ) );
            Prosite pattern = (Prosite)definition.Pattern;

            Assert.AreEqual("Prosite", definition.Name);
            Assert.AreEqual("prosite", pattern.Name);
            Assert.AreEqual("a-c-t", pattern.ToString());
            Assert.AreEqual(0.9, pattern.Impact, 1e-3);
        }

        [TestMethod]
        public void TestToXml()
        {
			Definition definition = DefinitionIO.Read( Global.GetResourceReader(   "BioPatMLXML/Prosite.xml" ) );

			Assert.IsTrue( definition.ToXml().ToString().IndexOf( "name=\"auto-" ) < 0 );
			Definition def2 = DefinitionIO.Read( DefinitionIO.Write( definition ) );

			Prosite pattern = (Prosite) def2.Pattern;

            Assert.AreEqual("Prosite", def2.Name);
            Assert.AreEqual("prosite", pattern.Name);
            Assert.AreEqual("a-c-t", pattern.ToString());
            Assert.AreEqual(0.9, pattern.Impact, 1e-3);
        }
    }
}
