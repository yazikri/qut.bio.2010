﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DB = System.Diagnostics.Debug;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using QUT.Bio.BioPatML.Sequences.Additional;
using QUT.Bio.BioPatML.Patterns;
using QUT.Bio.BioPatML.Sequences.List;
using QUT.Bio.BioPatML.Common.XML;
using Bio;

/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/
namespace TestBioPatML.TestPatterns
{
	[TestClass]
	public class TestProfileAll {
		[TestMethod]
		/** Tests the match method of a sequence of patterns */
		public void TestMatch1 () {
			ISequence seq = new Sequence( Alphabets.DNA, "taaacc" );//the original: baaacc
			ProfileAll pf = new ProfileAll();
			Motif motif;
			ProfileElement element;

			motif = new Motif( "motif1", Alphabets.DNA, "aa", 0.5 );
			element = pf.Add( motif );
			
            
            var matches = Searcher.SearchMatch(seq, 0, (int)seq.Count-1, pf );
			Assert.AreEqual( 4, matches.Count );
			Assert.AreEqual( "ta", matches[ 0 ].Letters() );//the original: ba
			Assert.AreEqual( 0, matches[ 0 ].Start );
			Assert.AreEqual( "aa", matches[ 1 ].Letters() );
			Assert.AreEqual( 1, matches[ 1 ].Start );
			Assert.AreEqual( "aa", matches[ 2 ].Letters() );
			Assert.AreEqual( 2, matches[ 2 ].Start );
			Assert.AreEqual( "ac", matches[ 3 ].Letters() );
			Assert.AreEqual( 3, matches[ 3 ].Start );
            
              
			motif = new Motif( "motif2", Alphabets.DNA, "acc", 0.5 );
			element = pf.Add( element, ProfileElement.AlignmentType.END, 0, 1, motif );
			
            
            matches = Searcher.SearchMatch(seq, 0, (int)seq.Count-1, pf );
			Assert.AreEqual( 3, matches.Count );
			Assert.AreEqual( "taaac", matches[ 0 ].Letters() );//the original: taaac
			Assert.AreEqual( 0, matches[ 0 ].Start );
			Assert.AreEqual( "taaacc", matches[ 1 ].Letters() );//the original: taaacc
			Assert.AreEqual( 0, matches[ 1 ].Start );
			Assert.AreEqual( "aaacc", matches[ 2 ].Letters() );
			Assert.AreEqual( 1, matches[ 2 ].Start );
            

			motif = new Motif( "motif3", Alphabets.DNA, "cc", 1.0 );
			element = pf.Add( element, ProfileElement.AlignmentType.END, -2, 1, motif );
			
            
            matches = Searcher.SearchMatch(seq, 0, (int)seq.Count-1, pf );
			Assert.AreEqual( 3, matches.Count );
			Assert.AreEqual( "taaacc", matches[ 0 ].Letters() );//the original: baaacc
			Assert.AreEqual( 0, matches[ 0 ].Start );
            Assert.AreEqual("taaacc", matches[1].Letters());//the original: baaacc
			Assert.AreEqual( 0, matches[ 1 ].Start );
			Assert.AreEqual( "aaacc", matches[ 2 ].Letters() );
			Assert.AreEqual( 1, matches[ 2 ].Start );
		}


		[TestMethod]
		/** Tests the match method of a sequence of patterns with different alignment */
		public void TestMatch2 () {
			Sequence seq = new Sequence( Alphabets.DNA, "taaacc" );//the original : baaacc
			ProfileAll pf = new ProfileAll();
			Motif motif;
			ProfileElement element;

			motif = new Motif( "motif1", Alphabets.DNA, "aa", 1.0 );
			element = pf.Add( motif );

            var matches = Searcher.SearchMatch(seq, 0, (int)seq.Count-1, pf);
			Assert.AreEqual( 2, matches.Count );
			Assert.AreEqual( 1, matches[ 0 ].Start );
			Assert.AreEqual( "aa", matches[ 0 ].Letters() );
			Assert.AreEqual( 2, matches[ 1 ].Start );
			Assert.AreEqual( "aa", matches[ 1 ].Letters() );
            

			motif = new Motif( "motif2", Alphabets.DNA, "taaa", 0.5 ); //the original baaa
			pf.Add( element, ProfileElement.AlignmentType.START, -1, 0, motif );
			
            
            matches = Searcher.SearchMatch(seq, 1, (int)seq.Count-1, pf );
			Assert.AreEqual( 3, matches.Count );
			Assert.AreEqual( 0, matches[ 0 ].Start );
			Assert.AreEqual( "taaa", matches[ 0 ].Letters() );//the original is baaa
			Assert.AreEqual( 1, matches[ 1 ].Start );
			Assert.AreEqual( "aaac", matches[ 1 ].Letters() );
			Assert.AreEqual( 1, matches[ 2 ].Start );
			Assert.AreEqual( "aaac", matches[ 2 ].Letters() );

             
			motif = new Motif( "motif3", Alphabets.DNA, "ac", 1.0 );
			pf.Add( element, ProfileElement.AlignmentType.CENTER, 0, 1, motif );
			
            
            matches = Searcher.SearchMatch(seq, 1, (int)seq.Count-1, pf );
			Assert.AreEqual( 3, matches.Count );
			Assert.AreEqual( 0, matches[ 0 ].Start );
			Assert.AreEqual( "taaac", matches[ 0 ].Letters() );//the original is baaac
			Assert.AreEqual( 1, matches[ 1 ].Start );
			Assert.AreEqual( "aaac", matches[ 1 ].Letters() );
			Assert.AreEqual( 1, matches[ 2 ].Start );
			Assert.AreEqual( "aaac", matches[ 2 ].Letters() );
		
             
        }

		[TestMethod]
		/** Tests the match method for a hierarchical pattern */
		public void TestMatch3 () {
			Sequence seq = new Sequence( Alphabets.DNA, "taaacc" );//the original is baaacc
			ProfileAll pf1 = new ProfileAll();
			ProfileAll pf2 = new ProfileAll();
			Motif motif;
			ProfileElement element;

			motif = new Motif( "motif1", Alphabets.DNA, "aa", 1.0 );
			element = pf1.Add( motif );
			motif = new Motif( "motif2", Alphabets.DNA, "aa", 1.0 );
			element = pf1.Add( element, ProfileElement.AlignmentType.START, 0, 1, motif );
			
            
            var matches = Searcher.SearchMatch(seq, 1, (int)seq.Count-1, pf1 );
			Assert.AreEqual( 3, matches.Count );
			Assert.AreEqual( "aa", matches[ 0 ].Letters() );
			Assert.AreEqual( "aaa", matches[ 1 ].Letters() );
			Assert.AreEqual( "aa", matches[ 2 ].Letters() );

			motif = new Motif( "motif3", Alphabets.DNA, "aa", 1.0 );
			element = pf2.Add( motif );
			motif = new Motif( "motif4", Alphabets.DNA, "cc", 1.0 );
			element = pf2.Add( element, ProfileElement.AlignmentType.CENTER, 0, 2, motif );
			matches = Searcher.SearchMatch( seq, 1, (int)seq.Count-1, pf2 );
			Assert.AreEqual( 2, matches.Count );
			Assert.AreEqual( "aaacc", matches[ 0 ].Letters() );
			Assert.AreEqual( "aacc", matches[ 1 ].Letters() );

			pf1.Add( pf1[0], ProfileElement.AlignmentType.END, -1, 0, pf2 );
			pf2.Threshold = ( 1.0 );
			matches = Searcher.SearchMatch( seq, 1, (int)seq.Count-1, pf1 );
			Assert.AreEqual( 2, matches.Count );
			Assert.AreEqual( "aaacc", matches[ 0 ].Letters() );
			Assert.AreEqual( "aaacc", matches[ 1 ].Letters() );
             
		}

        
		[TestMethod]
		/** Tests the match method for two patterns with a gap of zero */
        public void TestMatch4 () {
			Sequence seq = new Sequence( Alphabets.DNA, "taaacc" );//the original is baaacc
			ProfileAll pf = new ProfileAll();
			Motif motif;
			ProfileElement element;

            motif = new Motif("motif1", Alphabets.DNA, "aa", 1.0);
			element = pf.Add( motif );
            motif = new Motif("motif2", Alphabets.DNA, "cc", 1.0);
			element = pf.Add( element, ProfileElement.AlignmentType.END, 0, 0, motif );

            
			var matches = Searcher.SearchMatch( seq, 1, (int)seq.Count-1, pf );
			Assert.AreEqual( 1, matches.Count );
			Assert.AreEqual( "aacc", matches[ 0 ].Letters() );
			Assert.AreEqual( 2, matches[ 0 ].Start );
             
		}

		[TestMethod]
		/** Tests the match method for a pattern which matches all the time */
		public void TestMatch5 () {
            Sequence seq = new Sequence(Alphabets.DNA, "tacgt");//the original is bacgt
			ProfileAll pf = new ProfileAll();
			Motif motif;

            motif = new Motif("motif1", Alphabets.DNA, "aa", 0.0);
			pf.Add( motif );

            
			var matches = Searcher.SearchMatch( seq, 0, (int)seq.Count-1, pf );
			Assert.AreEqual( 4, matches.Count );
			Assert.AreEqual( "ta", matches[ 0 ].Letters() );//the original is ta
			Assert.AreEqual( 0, matches[ 0 ].Start );
			Assert.AreEqual( "ac", matches[ 1 ].Letters() );
			Assert.AreEqual( 1, matches[ 1 ].Start );
		
            
        }


	}
}
