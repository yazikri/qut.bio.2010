﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

using DB = System.Diagnostics.Debug;using Microsoft.VisualStudio.TestTools.UnitTesting;
using QUT.Bio.BioPatML.Common.XML;
using QUT.Bio.BioPatML.Sequences.Additional;
using QUT.Bio.BioPatML.Patterns;
using Bio;
using BioPatML.Test;

/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/
namespace TestBioPatML.TestPatterns
{
    [TestClass]
    public class TestMotif
    {
		/** test for match method from pattern interface */
		[TestMethod]
		public void MotifTestMatchAtStart () {
			ISequence seq = new Sequence( Alphabets.DNA, "aTtgattaca" );
			Motif motif = new Motif( "test", Alphabets.DNA, "attg", 0.0 );
			Match match = motif.Match( seq, 0 );

			Assert.AreEqual( 0, match.Start );
			Assert.AreEqual( 4, match.Count );
			//Assert.AreEqual( 1, match.Strand );
			Assert.AreEqual( 1.0, match.Similarity, 1e-2 );
			Assert.AreEqual( "attg", match.Letters() );
		}

		/** test for match method from pattern interface */
		[TestMethod]
		public void MotifTestTwoMatches () {
			Sequence seq = new Sequence( Alphabets.DNA, "aTtgattgaca" );
			Motif motif = new Motif( "test", Alphabets.DNA, "attg", 0.0 );
			Match match = motif.Match( seq, 0 );

			Assert.AreEqual( 0, match.Start );
			Assert.AreEqual( 4, match.Count );
			//Assert.AreEqual( 1, match.Strand );
			Assert.AreEqual( 1.0, match.Similarity, 1e-2 );
			Assert.AreEqual( "attg", match.Letters() );

			Match match2 = motif.Match( seq, 1 );
			Assert.AreEqual( 1, match.Start );
			Assert.AreEqual( 4, match.Count );
			//Assert.AreEqual( 1, match.Strand );
			Assert.AreEqual( 0.25, match.Similarity, 1e-2 );
			Assert.AreEqual( "ttga", match.Letters() );
		}

		/** test for match method from pattern interface */
		[TestMethod]
		public void MotifTestMatch () {
			Sequence seq = new Sequence( Alphabets.DNA, "aTtga" );
			Motif motif = new Motif( "test", Alphabets.DNA, "tgn", 0.0 );
			Match match = motif.Match( seq, 2 );

			Assert.AreEqual( 2, match.Start );
			Assert.AreEqual( 3, match.Count );
			//Assert.AreEqual( 1, match.Strand );
			Assert.AreEqual( 1.0, match.Similarity, 1e-2 );

			match = motif.Match( seq, 1 );
			Assert.AreEqual( 1, match.Start );
			Assert.AreEqual( 3, match.Count );
			//Assert.AreEqual( 1, match.Strand );
			Assert.AreEqual( 0.66, match.Similarity, 1e-2 );
		}

        /** test for match with alternatives */
        [TestMethod]
		public void MotifTestMatchAlternatives ()
        {
            Sequence seq = new Sequence(Alphabets.DNA, "atgc");
            Motif motif = new Motif("test", Alphabets.DNA,"[ga]tg[c]", 0.0);
            Match match = motif.Match(seq, 0);
            Assert.AreEqual("[ga]tg[c]", motif.Letters);
            Assert.AreEqual(1.0, match.Similarity, 1e-2);
        }

        /** test for match similarity */
        [TestMethod]
		public void MotifTestMatchSimiliarity ()
        {
            Sequence seq = new Sequence(Alphabets.DNA, "aCtG", true);

            Motif test = new Motif("test", Alphabets.DNA, "ct", 0);
            Match matchTest1 = test.Match(seq,1);

            Assert.AreEqual(2, matchTest1.Count);
            
            Motif motif = new Motif("test", Alphabets.DNA, "cc", 0);
            Match match = motif.Match(seq, 0);
            Assert.AreEqual(0.5, match.Similarity, 1e-3);
            Assert.AreEqual(1.0, (new Motif("test", Alphabets.DNA, "ct", 0)).Match(seq, 1).Similarity, 1e-3);

            Match matchTest2 = test.Match(seq, 2);
            Assert.AreEqual(0.0, matchTest2.Similarity, 1e-3);

            /*Circular test*/
            //Assert.AreEqual(1.0, (new Motif("test", Alphabets.DNA, "ctga", 0)).Match(seq, 1).Similarity, 1e-3);

            Motif seq1 = new Motif("test", Alphabets.DNA, "cc", 0.6);
            Assert.AreEqual(null, seq1.Match(seq, 0));
        }

        /** Tests the getter for the letters of a motif */
        [TestMethod]
		public void MotifTestLetters ()
        {
            Motif motif = new Motif("test", Alphabets.DNA, "tgn[at]C[TG]", 0.0);
            
            string letters = motif.Letters;
            Assert.AreEqual("tg[acgt][at]c[tg]", letters);
            
        }

        /** Tests the reading of a motif from an XML document */
        [TestMethod]
		public void MotifTestRead ()
        {
            Definition definition = DefinitionIO.Read(Global.GetResourceReader("BioPatMLXML/Motif.xml")); //ttgaca
            Motif pattern = null;
            pattern = (Motif)definition.Pattern;

            Assert.AreEqual("Motif", definition.Name);
            Assert.AreEqual("motif", pattern.Name);
            Assert.AreEqual(0.7, pattern.Threshold, 1e-3);
            Assert.AreEqual(0.9, pattern.Impact, 1e-3);
            Assert.AreEqual("ttgaca", pattern.Letters);
        }

        /** Tests the reading of a motif from an XML document */
        [TestMethod]
		public void TestToXml ()
        {
			Definition definition = DefinitionIO.Read( Global.GetResourceReader(  "BioPatMLXML/Motif.xml" ) );

			Assert.IsTrue( definition.ToXml().ToString().IndexOf( "name=\"auto-" ) < 0 );
			Definition def2 = DefinitionIO.Read( DefinitionIO.Write( definition ) );

			Motif pattern = (Motif) def2.Pattern;

            Assert.AreEqual("Motif", def2.Name);
            Assert.AreEqual("motif", pattern.Name);
            Assert.AreEqual(0.7, pattern.Threshold, 1e-3);
            Assert.AreEqual(0.9, pattern.Impact, 1e-3);
            Assert.AreEqual("ttgaca", pattern.Letters);
        }

    }
}
