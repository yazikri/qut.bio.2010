﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DB = System.Diagnostics.Debug;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QUT.Bio.BioPatML.Sequences;
using QUT.Bio.BioPatML.Patterns;
using QUT.Bio.BioPatML.Sequences.List;
using Bio;


/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/
namespace TestBioPatML.TestPatterns
{
	[TestClass]
	public class TestProfileBest {
		[TestMethod]
		public void TestMatch () {
			ISequence seq = new Sequence( Alphabets.DNA, "aaaacc" );
			ProfileBest pf = new ProfileBest( "test", 0.0 );
			Match Match;
			ProfileElement element;

            element = pf.Add(new Motif("motif1", Alphabets.DNA, "aa", 0));

			Match = pf.Match( seq, 1 );
			Assert.AreEqual( 1, Match.Start );
			Assert.AreEqual( 2, Match.Count );
			//Assert.AreEqual( 1, Match.Strand );
			Assert.AreEqual( 1.0, Match.Similarity, 1e-3 );
			Assert.AreEqual( "aa", Match.Letters() );

			Match = pf.Match( seq, 3 );
			Assert.AreEqual( 0.5, Match.Similarity, 1e-3 );
			Assert.AreEqual( "ac", Match.Letters() );

			pf.Add( element, ProfileElement.AlignmentType.END, -1, 1, new Motif( "motif2", Alphabets.DNA, "ac", 0 ) );
			Match = pf.Match( seq, 0 );
			Assert.AreEqual( 0, Match.Start );
			Assert.AreEqual( 5, Match.Count );
			//Assert.AreEqual( 1, Match.Strand );
			Assert.AreEqual( 1.0, Match.Similarity, 1e-3 );
			Assert.AreEqual( "aaaac", Match.Letters() );
			Assert.AreEqual( "aa", Match.SubMatches[0].Letters() );
			Assert.AreEqual( "ac", Match.SubMatches[1].Letters() );

			Match = pf.Match( seq, 2 );
			Assert.AreEqual( 2, Match.Start );
			Assert.AreEqual( 3, Match.Count );
			//Assert.AreEqual( 1, Match.Strand );
			Assert.AreEqual( 1.0, Match.Similarity, 1e-3 );
			Assert.AreEqual( "aac", Match.Letters() );

			Match = pf.Match( seq, 3 );
			Assert.AreEqual( 3, Match.Start );
			Assert.AreEqual( 3, Match.Count );
			//Assert.AreEqual( 1, Match.Strand );
			Assert.AreEqual( 0.5, Match.Similarity, 1e-3 );
			Assert.AreEqual( "acc", Match.Letters() );
			Assert.AreEqual( "ac", Match.SubMatches[0].Letters() );
			Assert.AreEqual( "cc", Match.SubMatches[1].Letters() );
		}

	}
}
