﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using DB = System.Diagnostics.Debug;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QUT.Bio.BioPatML.Sequences.Additional;
using QUT.Bio.BioPatML.Patterns;
using QUT.Bio.BioPatML.Common.XML;
using BioPatML.Test;
using Bio;

/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/
namespace TestBioPatML.TestPatterns
{
	[TestClass]
	public class 
        
        TestSetBest {
		[TestMethod]
		public void TestAddGetPattern () {
			SetBest set = new SetBest();
			Assert.AreEqual( 0, set.Count );

			Motif motif = new Motif( "motif1", Alphabets.DNA, "ACTG", 0.7 );
			set.Add( motif );
			Assert.AreEqual( 1, set.Count );
			Assert.AreEqual( motif, set[0] );
		}

		[TestMethod]
		/** Tests the matching a pattern set against a sequence */
		public void TestMatch () {
            
            ISequence seq = new Sequence(Alphabets.DNA, "atgcatgc");
            SetBest set = new SetBest("set1", 0.5);
            Motif motif1 = new Motif("motif1", Alphabets.DNA, "tgca", 0.0);
            
            set.Add(motif1);
            string letterbefore = motif1.Letters;
            Assert.AreEqual("tgca", motif1.Letters);

            set.Add(new Motif("", Alphabets.DNA, "tgc", 0.0));

            Assert.AreEqual("motif1", set[0].Name);
            string letter = motif1.Letters;
            Assert.AreEqual("tgca", motif1.Letters);
            Assert.AreEqual(motif1.Letters, ((Motif)set[0]).Letters);
            
            Match match = set.Match(seq, 1);
            
            Assert.AreEqual(1, match.Start);
            Assert.AreEqual(4, match.Count);
            //Assert.AreEqual( 1, match.Strand );
            Assert.AreEqual(1, match.Similarity, 1e-2);

            match = set.Match(seq, 0);
            Assert.AreEqual(null, match);

            match = set.Match(seq, 5);
            Assert.AreEqual(5, match.Start);
            Assert.AreEqual(3, match.Count);
            Assert.AreEqual(1, match.Similarity, 1e-2);
		
            
        }

		[TestMethod]
		/** Tests if the match method finds the best pattern */
		public void TestMatch1 () {
			ISequence seq = new Sequence( Alphabets.DNA, "taaacc" );
			SetBest set = new SetBest( "set1", 0.5 );

            Motif motif1 = new Motif("motif1", Alphabets.DNA, "taa", 0.0);

            set.Add(motif1);
            set.Add(new Motif("motif2", Alphabets.DNA, "aacc", 0.0));

            var matches = Searcher.SearchMatch(seq, 0, (int)seq.Count-1, set);
            Assert.AreEqual(3, matches.Count);
            Assert.AreEqual("taa", matches[0].Letters());
            
			Assert.AreEqual( 1.00, ( (Match) matches[ 0 ] ).Similarity, 1e-2 );
			Assert.AreEqual( "aaac", matches[ 1 ].Letters() );
			Assert.AreEqual( 0.75, ( (Match) matches[ 1 ] ).Similarity, 1e-2 );
			Assert.AreEqual( "aacc", matches[ 2 ].Letters() );
			Assert.AreEqual( 1.00, ( (Match) matches[ 2 ] ).Similarity, 1e-2 );
		
            }

		[TestMethod]
		public void TestRead () {
			Definition definition = DefinitionIO.Read( Global.GetResourceReader(   "BioPatMLXML/SetBest.xml" ) );
			SetBest pattern = (SetBest) definition.Pattern;

			Assert.AreEqual( "setbest", pattern.Name );
			Assert.AreEqual( 0.7, pattern.Threshold, 1e-3 );
			Assert.AreEqual( 0.9, pattern.Impact, 1e-3 );

			Assert.AreEqual( "motif1", pattern[0].Name );
			Assert.AreEqual( "motif2", pattern[1].Name );
			Assert.AreEqual( "regex1", pattern[2].Name );
		}

		[TestMethod]
		public void TestToXml () {
			Definition definition = DefinitionIO.Read( Global.GetResourceReader(   "BioPatMLXML/SetBest.xml" ) );

			Assert.IsTrue( definition.ToXml().ToString().IndexOf( "name=\"auto-" ) < 0 );
			Definition def2 = DefinitionIO.Read( DefinitionIO.Write( definition ) );

			SetBest pattern = (SetBest) def2.Pattern;

			Assert.AreEqual( "setbest", pattern.Name );
			Assert.AreEqual( 0.7, pattern.Threshold, 1e-3 );
			Assert.AreEqual( 0.9, pattern.Impact, 1e-3 );

			Assert.AreEqual( "motif1", pattern[0].Name );
			Assert.AreEqual( "motif2", pattern[1].Name );
			Assert.AreEqual( "regex1", pattern[2].Name );
		}
	}
}
