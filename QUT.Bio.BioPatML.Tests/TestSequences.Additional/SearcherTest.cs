﻿using QUT.Bio.BioPatML.Sequences.Additional;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Bio;
using QUT.Bio.BioPatML.Patterns;
using System.Collections.Generic;

namespace QUT.Bio.BioPatML.Tests
{
    
    
    /// <summary>
    ///This is a test class for SearcherTest and is intended
    ///to contain all SearcherTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SearcherTest
    {

        [TestMethod]
        public void TestSearch()
        {
            ISequence seq = new Sequence(Alphabets.DNA, "acttacttagttaac");
            Motif motif = new Motif("motif1", Alphabets.DNA, "act", 0.5);

            var list = Searcher.SearchMatch(seq, 0, 0, motif);
            Assert.AreEqual(3, list.Count);
            Assert.AreEqual(0, list[0].Start);
            Assert.AreEqual(1.0, ((Match)list[0]).Similarity, 1e-1);
            Assert.AreEqual(4, list[1].Start);
            Assert.AreEqual(1.0, ((Match)list[1]).Similarity, 1e-1);
            Assert.AreEqual(8, list[2].Start);
            Assert.AreEqual(0.6, ((Match)list[2]).Similarity, 1e-1);

            list = Searcher.SearchMatch(seq, 13, (int)seq.Count, motif);
            Assert.AreEqual(1, list.Count);
            Assert.AreEqual(13, list[0].Start);
            Assert.AreEqual(0.6, ((Match)list[0]).Similarity, 1e-1);


        }
    }
}
