﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio;

/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/
namespace QUT.Bio.BioPatML.Sequences.List
{
	/// <summary> This class describes a list of sequences. </summary>
	public class SequenceList<T> : List<T> where T : ISequence {
        private string name;

		/// <summary> Creates an empty sequence list. </summary>

		public SequenceList ()
			: base() 
        { 
        }

		/// <summary>
		///  Creates an empty sequence list with the given name.
		/// </summary>
		/// <param name="name"></param>

		public SequenceList ( String name )
        {
            this.name = name;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int MinLength()
        {
            return this.Min( sequence => (int)sequence.Count);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public  int MaxLength()
        {
            return this.Max( sequence => (int)sequence.Count);
        }

    }

    /// <summary> This class describes a list of sequences. </summary>
	
    public class SequencesList : SequenceList<ISequence> 
    {
        /// <summary> Creates an empty sequence list. </summary>

        public SequencesList()
            : base() { /* No implementation */ }

        /// <summary>
        ///  Creates an empty sequence list with the given name.
        /// </summary>
        /// <param name="name"></param>

        public SequencesList(String name)
            : base(name) { /* No implementation */ }

        
    }
}
