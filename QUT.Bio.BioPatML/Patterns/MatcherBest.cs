﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using QUT.Bio.BioPatML.Sequences.Additional;
using QUT.Bio.BioPatML.Common.XML;
using Bio;

/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/
namespace QUT.Bio.BioPatML.Patterns
{
    /// <summary>
    /// This class defines a composition pattern. A composition pattern describes
    /// the symbol composition of a sequence section of variable length.
    /// </summary>

    public partial class Composition : PatternFlexible
    {
        /// <summary> Matcher to find the best match/length of the composition. 
        /// </summary>

        private class MatcherBest : IMatcher
        {
            #region -- Automatic Properties --

            /// <summary>
            /// The composition pattern which the matcher feeds on.
            /// </summary>
            private Composition Composition 
            { 
                get; 
                set; 
            }


            #endregion


            #region -- Constructor --
            
            
            /// <summary>
            /// Main constructor for building matcher implementation on our composition pattern.
            /// </summary>
            /// <param name="composition">Composition which the matcher sits on.</param>
            
            public MatcherBest(Composition composition)
            {
                Composition = composition;
            }


            #endregion


            #region -- IMatcher Members --
            
            /// <summary>
            /// Gets the position increment after matching a pattern. Some pattern
            /// can match several times with different length at the same position. In
            /// this case the increment is zero until all matches are performed. For some
            /// patterns an increment greater than one can be performed, e.g. string
            /// searching with the Boyer-Moore algorithm. 
            /// </summary>
            
            public int Increment
            {
                get 
                { 
                    return 1; 
                }
            }


            /// <summary>
            /// Returns the matched object of our composition pattern
            /// </summary>
            
            public Match MatchResult
            {
                get
                {
                    return Composition.LatestMatch;
                }
            }


            /// <summary>
            /// Implementation of the IMatcher interface. An any pattern matches any sequence.
            /// <see cref="QUT.Bio.BioPatML.Patterns.IMatcher">IMatcher interface</see>.
            /// </summary>
            /// <param name="sequence">Sequence to compare with.</param>
            /// <param name="position">Matching position.</param>
            /// <returns>The matched item</returns>
            
            public Match Match(ISequence sequence, int position)
            {
                Match match = MatchResult;
                int maxLen = Composition.MaxLength;
                int minLen = Composition.MinLength;
                double incLen = Composition.IncLength;

                double sum = 0.0;

                for (int len = 0; 
                    len < minLen && position + len < sequence.Count; 
                    len++
                    )
                    sum += Composition.Weight((char)sequence[(long)(position + len)]);
                
                double bestSum = sum;
                int bestLen = minLen;
                
                for (int len = minLen; 
                    len < maxLen && position + len < sequence.Count; 
                    len++
                    )
                {
                    sum += Composition.Weight((char)sequence[(long)(position + len )]);
                    
                    double diff = len - minLen + 1;
                    double rest = Math.Abs(diff - incLen * (int)(diff / incLen + 0.5));
                    
                    if (sum / (len + 1) >= bestSum / (bestLen+1) && rest < 0.5)
                    {
                        bestSum = sum;
                        bestLen = len + 1;
                    }
                }

                double similarity = bestSum / (bestLen  * Composition.MaxWeight);
                
                if (similarity < Composition.Threshold)
                    return null;

                match.Set(sequence, position, bestLen , Strand.Forward, similarity);
                
                return match;
            }

            #endregion
        }
    }
}
