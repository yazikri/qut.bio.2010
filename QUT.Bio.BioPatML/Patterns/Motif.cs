﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using QUT.Bio.BioPatML.Common.XML;
using QUT.Bio.BioPatML.Sequences.Additional;
using Bio;
using QUT.Bio.BioPatML.Alphabet;



/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/

namespace QUT.Bio.BioPatML.Patterns
{
	
    
    /// <summary>
	/// This class implements a pattern described by a motif sequence. Alternative
	/// symbols at a position are allowed.
	/// </summary>
	public class Motif : Pattern 
    {

        private List<int> motifSymbolMap = new List<int>();


		/// <summary> The alphabet used for this motif.
		/// </summary>
		private IAlphabet alphabet;


        /// <summary> List of symbols in this motif element
		/// </summary>
        private List<char> motifSymbols = new List<char>();


        /// <summary> Internal constructor for deserialization.
		/// </summary>
        public Motif () 
        { 
            /*no implementation*/
        }


		/// <summary> Creates a new motif pattern. The motif is provided as a letter string
		/// </summary>
		/// <param name="name">Name of the pattern.</param>
		/// <param name="alphabetType">Name of the alphabet of the motif.</param>
		/// <param name="motif">Motif description.</param>
		/// <param name="threshold">The minimum similarity threshold required for a match.</param>
        public Motif (
			String name,
			IAlphabet alphabetType,
			String motif,
			double threshold
		)
			: base( name ) 
        {
			Threshold = threshold;
			alphabet = alphabetType;
			ParseSymbols( motif);
        }


		/// <summary> Creates a new anonymous motif with default threshold 1.0.
		/// </summary>
		/// <param name="alphabetType">The Alphabet from which the symbols in the motif are drawn.</param>
		/// <param name="motif">The motif.</param>
		/// <param name="threshold">The minimum similarity threshold required for a match.</param>
        public Motif (
			IAlphabet alphabetType,
			char motif,
			double threshold = 1.0
		) 
			: this ( null, alphabetType, motif.ToString(), threshold )
		{
		
        }

        
        /// <summary> Gets the letters of the motif pattern.
		/// </summary>
		public String Letters 
        {
			get 
            {
				StringBuilder sb = new StringBuilder();
                int idx = 0; //index of motifSymbols

                for (int i = 0; i < motifSymbolMap.Count; i++ )
                {
                    switch (motifSymbolMap[i])
                    {
                        case OpenBracket: 
                                sb.Append("["); 
                                break;

                        case AlternativeSymbol:
                                sb.Append( motifSymbols[idx] );
                                idx++;
                                break;
                        
                        case BasicSymbol :
                                BuildBasicSymbolLetters( motifSymbols, idx, ref sb);
                                idx++;
                                break;

                        case CloseBracket:
                                sb.Append("]");
                                break;

                        default: break;
                    }
                }

               return sb.ToString();
			}
		}


        /// <summary>
        /// Build the basic symbol letter, append to the string builder and pass via reference 
        /// </summary>
        /// <param name="motifSymbols">The collection of symbols from the motif </param>
        /// <param name="idx">The index of motifSymbols</param>
        /// <param name="sb">The string builder for build the motif letter</param>
        private void BuildBasicSymbolLetters(List<char> motifSymbols, int idx, ref StringBuilder sb)
        {
            if (alphabet.CheckIsAmbiguous((byte)motifSymbols[idx]))
            {
                HashSet<byte> basicSymbols;

                if (SuccessGetBasicSymbols(motifSymbols[idx], out basicSymbols))
                {
                    sb.Append("[");

                    for (int l = 0; l < basicSymbols.Count; l++)
                    {
                        sb.Append(char.ToLower((char)basicSymbols.ElementAt(l)));
                    }

                    sb.Append("]");
                }
                else
                    sb.Append(motifSymbols[idx]);

            }
            else
            {
                sb.Append(motifSymbols[idx]);
            }
                            
        }


		/// <summary> Implementation of the IMatcher interface. An any pattern matches any sequence.
		/// <see cref="QUT.Bio.BioPatML.Patterns.IMatcher">IMatcher interface</see>.
		/// </summary>
		/// <param name="sequence">Sequence to compare with.</param>
		/// <param name="position">Matching position.</param>
		/// <returns>A match object containning the search result</returns>
        public override Match Match (
			ISequence sequence,
			int position
		) 
        {
            int length = CountLength(),
                maxMismatches = (int) ( length * ( 1 - Threshold ) ),
                seqIdx = position,
                motifIdx = 0; 

            int[] score = InitializeScore ( motifSymbols );

            
            for ( int i = 0; i < motifSymbolMap.Count; i++ )
            {
                if ( seqIdx >= sequence.Count || motifIdx >= motifSymbols.Count)
                    break;
                
                byte seqByte = ( byte ) char.ToUpper ( ( char ) sequence[(long)seqIdx] );
                
                switch ( motifSymbolMap[i] )
                {
                    case OpenBracket: 
                        break;
                    
                    case CloseBracket: 
                        break;
                    
                    case BasicSymbol: 
                        MatchBasicSymbols (seqByte, ref motifIdx, ref seqIdx, ref score );
                        break;

                    case AlternativeSymbol:
                        MatchAlternativeSymbol ( sequence, i, seqByte, ref motifIdx, ref seqIdx, ref score );

                        if (seqIdx + 1 <= sequence.Count && motifSymbolMap[i + 1] != AlternativeSymbol) 
                            seqIdx++; 

                        motifIdx++;
                        
                        break;

                    default: break;
                
                }
            }

            int missMatches = CountMismatches (score );

            double similarity = (double)(length - missMatches) / length; 

            if (missMatches > maxMismatches || similarity < Threshold)
                return null;
            
            LatestMatch.Set(
                sequence, 
                position, 
                length, 
                Strand.Forward,
                similarity
                );
            
            return LatestMatch;
		}

        /// <summary>
        /// Return the length of some motif. The symbol inside the motif is counted as 1.
        /// For example:
        /// a motif TT[AT]AT has length 5 instead of 6
        /// </summary>
        /// <returns></returns>
        private int CountLength()
        {
            int length = 0;
            for (int i = 0; i < motifSymbolMap.Count; i++ )
            {
                if(motifSymbolMap[i] == BasicSymbol || motifSymbolMap[i] == OpenBracket)
                    length++;
            }
            return length;
        }


        /// <summary>
        /// Count the number of mismatch between the motif and the sequence
        /// </summary>
        /// <param name="motifSymbols">The collection of symbols in the motif</param>
        /// <param name="score">The collection of score of matching position. 
        /// 1 means matching and 0 means missmatching</param>
        /// <returns>Total mismatch between the motif and sequence</returns>
        private int CountMismatches(int[] score)
        {
            const int mismatch = 0;
            int mismatchScore = 0, idxScore = 0;

            for ( int i = 0; i < motifSymbolMap.Count; i++ )
            {
                switch(motifSymbolMap[i])
                {
                    case OpenBracket:
                        break;
                    case CloseBracket:
                        if (score[idxScore-1] == mismatch) mismatchScore ++;
                        break;
                    case BasicSymbol:
                        if (score[idxScore] == mismatch) mismatchScore ++;
                        idxScore++;
                        break;
                    case AlternativeSymbol:
                        idxScore++;
                        break;
                    default: break;
                    
                }
            }

            return mismatchScore;

        }


        /// <summary>
        /// Initialize the score array for matching between the motif and the sequence 
        /// </summary>
        /// <param name="motifSymbols">The collection of symbols from the motif </param>
        /// <returns>The array of matching score</returns>
        private int[] InitializeScore ( List<char> motifSymbols )
        {
            int[] score = new int[motifSymbols.Count];

            for ( int i = 0; i < motifSymbols.Count; i++ )
                score[i] = 0;

            return score;
        }


        
        /// <summary>
        /// Match the alternative symbols (the symbols inside the bracket) in the motif with the sequence.
        /// </summary>
        /// <param name="seq">Sequence to compare with </param>
        /// <param name="mapIdx">The index number in the map symbols</param>
        /// <param name="seqByte">The symbol of sequence in byte version</param>
        /// <param name="motifIdx">the position index in the motif array</param>
        /// <param name="seqIdx">the position number in the sequence</param>
        /// <param name="score">The score array for matching between the motif and the sequence </param>
        private void MatchAlternativeSymbol (
            ISequence seq, 
            int mapIdx, 
            byte seqByte, 
            ref int motifIdx, 
            ref int seqIdx, 
            ref int[] score
            )
        {
            if (score[motifIdx] == 1)
                return;

            if (alphabet.CompareSymbols(seqByte, (byte)motifSymbols[motifIdx]))
            {
                score[motifIdx] = 1;

                if (PreviousSymbolIsAlternative(motifIdx, mapIdx, score, motifSymbolMap))
                    score[motifIdx - 1] = 1;

                if (NextSymbolIsAlternative(seqIdx, seq, mapIdx, motifSymbolMap))
                    score[motifIdx + 1] = 1;

            }
        
        }


        /// <summary>
        /// Check whether the next symbol in the motif is the alternative symbol
        /// </summary>
        /// <param name="seqIdx">the position number in the sequence</param>
        /// <param name="seq">the Sequence to compare with.</param>
        /// <param name="mapIdx">The index number in the map symbols</param>
        /// <param name="motifSymbolMap">The map of symbol in the motif</param>
        /// <returns>True if the next symbol is the alternative symbol</returns>
        private bool NextSymbolIsAlternative( int seqIdx, ISequence seq, int mapIdx, List<int> motifSymbolMap )
        {
            return (
                seqIdx + 1 < seq.Count && 
                mapIdx + 1 < motifSymbolMap.Count && 
                motifSymbolMap[mapIdx + 1] == AlternativeSymbol
                );
        }


        /// <summary>
        /// Check whether the previous symbol in the motif is the alternative symbol
        /// </summary>
        /// <param name="motifIdx">the position index in the motif array</param>
        /// <param name="mapIdx">The index number in the map symbols</param>
        /// <param name="score">The score array for matching between the motif and the sequence </param>
        /// <param name="motifSymbolMap">The map of symbol in the motif</param>
        /// <returns>True if the previous symbol in the motif is the alternative symbol</returns>
        private bool PreviousSymbolIsAlternative( 
            int motifIdx, 
            int mapIdx, 
            int[] score, 
            List<int> motifSymbolMap )
        {
            return (
                motifIdx - 1 >= 0 && 
                mapIdx - 1 >= 0 && 
                score[motifIdx] == 1 && 
                motifSymbolMap[mapIdx - 1] == AlternativeSymbol
                );
        }


        /// <summary>
        /// Match the basic symbols (the symbols outside the bracket) in the motif with the sequence.
        /// </summary>
        /// <param name="seqByte">The symbol of sequence in byte version</param>
        /// <param name="motifIdx">the position index in the motif array</param>
        /// <param name="seqIdx">the position number in the sequence</param>
        /// <param name="score">The score array for matching between the motif and the sequence </param>
        private void MatchBasicSymbols(
            byte seqByte, 
            ref int motifIdx, 
            ref int seqIdx,
            ref int[] score
            )
        {

            if ( IsAmbiguous ( motifSymbols[motifIdx] ) )
            {
                HashSet<byte> basicSymbols;

                if (SuccessGetBasicSymbols(motifSymbols[motifIdx], out basicSymbols))
                {
                    int scoreAmbiguous = 0;
                    for (int idx = 0; idx < basicSymbols.Count; idx++)
                    {
                        if (alphabet.CompareSymbols(seqByte, basicSymbols.ElementAt(idx)))
                        {
                            scoreAmbiguous++;
                        }
                    }

                    if (scoreAmbiguous > 0)
                    {
                        score[motifIdx] = 1;
                    }

                }
            }
            else
            {
                if (alphabet.CompareSymbols ( seqByte, ( byte ) motifSymbols[motifIdx] ) )
                {
                    score[motifIdx] = 1;
                }
            }

            seqIdx++;
            motifIdx++;
        }


        /// <summary>
        /// Translate the ambiguous symbol to the basic symbols
        /// </summary>
        /// <param name="symbol">The result of translation ambiguos symbols</param>
        /// <param name="ambiguousSymbols">the collection of ambiguous symbols</param>
        /// <returns>True if the ambiguous symbol success to translated</returns>
        private bool SuccessGetBasicSymbols ( char ambiguousymbol, out HashSet<byte> basicSymbols )
        {
            IAlphabet ambiguousAlphabet = AlphabetConversion.TurnToAmbiguous( alphabet );

            return ambiguousAlphabet.TryGetBasicSymbols(
                (byte)char.ToUpper(ambiguousymbol), 
                out basicSymbols
                );
                            
        }


        /// <summary>
        /// Check whether the specific symbols is ambigous
        /// </summary>
        /// <param name="symbol"> The specific symbols to be checked</param>
        /// <returns>True if the specific symbols is ambigous</returns>
        private bool IsAmbiguous ( char symbol )
        {
            return alphabet.CheckIsAmbiguous((byte)symbol);
        }


		/// <summary> Parses the motif description and generates a symbol array that describes
		/// the motif. Alternatives are described by MetaSymbols.
		/// </summary>
		/// <param name="motif">Motif description.</param>
		/// <returns>Returns a symbol array.</returns>
        public void ParseSymbols ( String motif ) 
        {

			Parse( motif, motifSymbols, motifSymbolMap, alphabet );
		}


        /// <summary> Parse a ssequence of characters into a list of Symbol.
		/// </summary>
		/// <param name="sequence"></param>
		/// <param name="symbols"></param>
		/// <param name="alphabet"></param>
        public static void Parse(
			string motif,
			List<char> symbols,
			List<int> map,
            IAlphabet alphabet
		) {
            int length = motif.Length;
            bool alternative = false;
            symbols.Clear();
            
            //intialize
            for (int i = 0; i < length; i++)
            {
                map.Add(BasicSymbol);
            }

            
            for (int j = 0; j < motif.Length; j++)
            {
                char letter = char.ToLower( motif[j] );
                
				if ( letter == '[' )  
                {
					if (alternative ) 
                        throw new ArgumentException( "'[' within alternative is not permitted" );
					
                    map[j] = OpenBracket;
                    alternative = true;
                    
                }
                else 
                {
					if ( letter == ']' ) 
                    {
						if ( !alternative ) 
                            throw new ArgumentException( "Opening bracket for ']' is missing!" );
						
                        map[j] = CloseBracket;
                        alternative = false;
                    }
                    else if ( alternative ) 
                    {
                        map[j] = AlternativeSymbol;
                        symbols.Add(letter);
                    }
					else  
                    {
                        map[j] = BasicSymbol;
                        symbols.Add(letter);
                        
                    }
				}
			}

			if ( alternative ) 
				throw new ArgumentException( "']' is missing" );

        }

        


		/// <summary> Reads the parameters and populate the attributes for this pattern.
		/// </summary>
		/// <param name="element">Motif Pattern node</param>
		/// <param name="definition">The container encapsulating this pattern</param>
        public override void Parse (
			XElement element,
			Definition definition
		) 
        {
			base.Parse( element, definition );
            
            alphabet = AlphabetConversion.Convert(element.EnumValue<AlphabetType>("alphabet"));
			string motif = element.String( "motif" );

			if ( motif == null ) 
				throw new ArgumentException( "Required attribute 'motif' is missing in Motif." );
			
			ParseSymbols( motif );
		}


		/// <summary> Saves the contents of this object in an xml element.
		/// </summary>
		/// <returns>An xml element containign the content of this object.</returns>
        public override XElement ToXml () {
			return base.ToXml( "Motif",
				new XAttribute( "alphabet", alphabet.Name.ToUpper() ),
				new XAttribute( "motif", Letters )
			);
		}

    }
}
