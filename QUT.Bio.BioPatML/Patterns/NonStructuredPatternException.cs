﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/


namespace QUT.Bio.BioPatML.Patterns 
{
	/// <summary> Exception used to indicate that a pattern does not support child patterns. </summary>

	public class NonStructuredPatternException : Exception 
    {

		/// <summary> Create a new NonStructuredPatternExcception object. </summary>
		
		public NonStructuredPatternException () : base( "Non-structured pattern: child patterns not supported." ) { }
	}
}
