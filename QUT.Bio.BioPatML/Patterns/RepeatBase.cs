﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using QUT.Bio.BioPatML.Sequences.Additional;
using QUT.Bio.BioPatML.Common.XML;
using Bio;

/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/

namespace QUT.Bio.BioPatML.Patterns
{

	/// <summary>
	///  This class implements the repeat pattern. A repeat pattern consists of a
	///  reference pattern and a repeat element pattern which is the repeat of
	///  the reference pattern. More complex repeats can be built by using profiles
	///  and repeat element patterns directly. See {RepeatElement} for an example.
	/// </summary>
	
    public partial class Repeat : Pattern 
    {

		/// <summary>
		/// Base class for repeats.
		/// </summary>
		
        private abstract class MatcherRepeat : IMatcher 
        {
			
            #region -- Fields --
			
            /// <summary>
			/// ref of the outter repeat element
			/// </summary>
			
            protected Repeat repeat;
			
            
            /// <summary>
			/// Matching sequence
			/// </summary>
			
            protected ISequence matchSeq;
			
            
            /// <summary>
			/// Number of element in our matchSeq
			/// </summary>
			
            protected int matchLen;


			protected double remSim;
			
            
            #endregion


			#region -- Constructor --
			

            /// <summary>
			/// The base constructor
			/// </summary>
			/// <param name="repeat">Reference of the repeat element</param>
			
            public MatcherRepeat ( Repeat repeat ) 
            {
				this.repeat = repeat;
			}


			/// <summary>
			/// Inits the matching procedure in the derived classes.
			/// </summary>
			
            protected void Init () 
            {
                Match latestMatch = repeat.ReferencedPattern.LatestMatch;

                long length;

                if (latestMatch.End >= latestMatch.BaseSequence.Count)
                {
                    length = latestMatch.BaseSequence.Count - latestMatch.Start ;
                }
                else
                {
                    length = latestMatch.End - latestMatch.Start + 1;   
                }
                
                matchSeq = latestMatch.BaseSequence.GetSubSequence(latestMatch.Start, length);
                
                matchLen = ( matchSeq == null ? 0 : (int)matchSeq.Count );
				remSim = matchLen;
			}


			#endregion


			#region -- Protected Methods --
			
            
            /// <summary>
			/// Compares two symbol and returns the "similarity" (weight) between the symbols
			/// </summary>
			/// <param name="symbol1">First symbol.</param>
			/// <param name="symbol2">Second symbol.</param>
			/// <returns>Returns the "similarity" (weight) between the symbols.</returns>
			
            protected double Compare
                (char symbol1, char symbol2)
            {
                const double equalWeight = 1.0,
                             notEqualWeight = 0.0;

                if (repeat.weights == null)
                    return symbol1.Equals(symbol2) ? equalWeight : notEqualWeight;

				return repeat.Weight( symbol1, symbol2 );
			}


			#endregion


			#region IMatcher Members

            /// <summary>
            /// 
            /// </summary>
            /// <param name="sequence"></param>
            /// <param name="position"></param>
            /// <returns></returns>
			
            public virtual Match Match ( ISequence sequence, int position ) 
            {
				return matchSeq == null ? null : repeat.LatestMatch;
			}


            /// <summary>
            /// Return the latest match if the match sequence is exist
            /// </summary>
			
            public Match MatchResult 
            {
				get 
                { 
                    return matchSeq == null ? null : repeat.LatestMatch; 
                }
			}


            /// <summary>
            /// 
            /// </summary>
			public int Increment 
            {
				get 
                { 
                    return 1; 
                }
			}

			#endregion
			
		}
	}
}
