﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using QUT.Bio.BioPatML.Sequences.Additional;
using QUT.Bio.BioPatML.Common.XML;
using Bio;


/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/

namespace QUT.Bio.BioPatML.Patterns
{

    /// <summary>
    /// Part of Repeat class
    /// </summary>
    
    public partial class Repeat : Pattern
    {

        /// <summary>
        /// The direct matcher for our repeat element
        /// </summary>
        
        private class MatcherDirect : MatcherRepeat
        {


            #region -- Constructor --
            
            /// <summary>
            /// Default constructor
            /// </summary>
            /// <param name="repeat"></param>
            
            public MatcherDirect(Repeat repeat)
                : base(repeat) 
            { 
            }


            #endregion


            #region -- IMatcher Implementation --


            /// <summary>
            /// The direct Match algorithm for matching
            /// </summary>
            /// <param name="sequence">sequence to compare</param>
            /// <param name="position">matching position</param>
            /// <returns></returns>
            
            public override Match Match(ISequence sequence, int position)
            {
                Init();

                remSim = 0;
                for (int i = 0; i < matchLen; i++)
                {
                    double compared = Compare((char)matchSeq[i], (char)sequence[position + i]);
                    remSim = remSim + compared;

                    if (i == matchLen - 1 && remSim / matchLen < repeat.Threshold)
                        return null;
                }

                repeat.LatestMatch.Set(sequence, position, matchLen, Strand.Forward, remSim / matchLen);

                return repeat.LatestMatch;
            }

            #endregion

        }
    }
}
