﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QUT.Bio.BioPatML
{
    /// <summary>
    /// This class keeps track of the version 
    /// </summary>  
    public static class Version
    {
        public static string History() 
        {
            return "BioPatML .Net Bio Version History\n" +
                    "Version    Date          Comment   \n" +
                    "1.00       10.11.2013     BioPatML .NET Bio first version\n" +
                    "0.00       n/a            Translated from Jacobi (Java)\n" ;
        }



        public static string JacobiHistory() 
        {
            return  "Jacobi (Java) Version history:\n" +
                    "Version   Date          Comment   \n" +
                      "1.12      25.12.2006    Modifications Repeat pattern and pattern interface\n" +
                      "1.11      12.11.2006    Little extensions: GSI, HMM interface\n" +
                      "1.10      01.11.2006    Symbol iterator for sequences, get() -> symbol()\n" +
                      "1.09      14.10.2006    Package prefix changed: edu.au -> au.edu.\n" +
                      "1.08      20.08.2006    Graph package to display sequences.\n" +
                      "1.07      10.08.2006    Block pattern wasn't loaded.\n" +
                      "1.06      20.06.2006    Bug in GenBankReader fixed. Header wasn't stored.\n" +
                      "1.05      17.06.2006    Patterns extended. get()-> extractor. Extractor package added.\n" +
                      "1.04      25.03.2006    Simplification of sequence constructors.\n" +
                      "1.03      08.12.2005    GenBankReader: Location over several lines.\n" +
                      "1.01      11.11.2005    weights for patterns.\n" +
                      "1.00      08.11.2005    First version.\n";
        }
    }
}
