﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using QUT.Bio.BioPatML.Sequences.List;
using Bio.IO;
using Bio;
using Bio.IO.FastA;

/*****************| Queensland University Of Technology |********************
 *  Original Author          : Samuel Toh (Email: yu.toh@connect.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrance BuckingHam
 * 
 ***************************************************************************/

namespace QUT.Bio.BioPatML.Readers {
	/// <summary>
	/// Class for reading fasta sub sequence file. The implementation is quite similiar to
	/// genbank file parser.
	/// </summary>
	public sealed class BioFastaReader : ReaderBase {
		#region -- Constructor --
		/// <summary>
		/// Default empty fasta parser constructor
		/// </summary>
		public BioFastaReader () : base() { }
		#endregion

		#region -- Method --

		#endregion -- Method --

		#region -- Read Method for Fasta file --

		
		/// <summary>
		/// Reads in the fasta file.
		/// </summary>
		/// <param name="reader">your local filepath for genbank</param>
		/// <returns>list of BioPatML Sequences</returns>
		public override SequencesList Read ( string filePath ) {
			//Create the parser first
            ISequenceParser fastaParser = new FastAParser(filePath);

			SequencesList bioSeqList = new SequencesList();

			foreach ( ISequence seq in fastaParser.Parse() ) {
				bioSeqList.Add( seq );
			}

			return bioSeqList;
		}

        /// <summary>
        /// Reads in the fasta file.
        /// </summary>
        /// <param name="reader">your local filepath for genbank</param>
        /// <returns>list of BioPatML Sequences</returns>
        public override SequencesList Read(StreamReader reader)
        {
            //Create the parser first
            ISequenceParser fastaParser = new FastAParser();

            IEnumerable<ISequence> mbfSequences = fastaParser.Parse(reader);

            SequencesList bioSeqList = new SequencesList();

            foreach (Sequence seq in mbfSequences)
            {
                bioSeqList.Add(seq);
            }

            return bioSeqList;
        }

		#endregion
	}
}
