﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio;

namespace QUT.Bio.BioPatML.Alphabet
{
    /// <summary>
    /// This class handle the conversion of type alphabet between BioPatML and .NET Bio as well as within .NET Bio
    /// </summary>
    public static class AlphabetConversion
    {
        /// <summary>
        /// This method is essential for convert alphabet in Alphabet Type in BioPatML to the alphabet version
        /// in .NET Bio. The usage of this class is mainly in parsing the element of alphabet in 
        /// XML files .
        /// </summary>
        /// <param name="type">The alphabet type members of enum AlphabetType</param>
        /// <returns>an alphabet object in .NET Bio</returns>
        public static IAlphabet Convert(AlphabetType type) 
        { 
            switch(type)
            {
                case AlphabetType.DNA: return DnaAlphabet.Instance;
                case AlphabetType.RNA: return RnaAlphabet.Instance;
                case AlphabetType.AA: return ProteinAlphabet.Instance;
                default: return DnaAlphabet.Instance;
            }
        }

        

        /// <summary>
        /// Convert the basic alphabet into the ambiguous version in .NET Bio.
        /// The ambigous version of basic Alphabet is essential for retrieve the basic symbol for 
        /// a given ambiguous symbol.
        /// ex: 
        /// IAlphabet ambigousAlpha = AlphabetConversion.TurnToAmbiguous(Sequence.Alphabet);
        /// 
        /// </summary>
        /// <param name="alphabet">The basic alphabet </param>
        /// <returns>Return the ambiguous version of alphabet</returns>
        public static IAlphabet TurnToAmbiguous(IAlphabet alphabet) 
        {

            if (alphabet == DnaAlphabet.Instance)
            {
                return AmbiguousDnaAlphabet.Instance;
            }
            else if (alphabet == RnaAlphabet.Instance)
            {
                return AmbiguousRnaAlphabet.Instance;
            }
            else if (alphabet == ProteinAlphabet.Instance)
            {
                return AmbiguousProteinAlphabet.Instance;
            }
            else 
            {
                return alphabet;
            }
        }


        /// <summary>
        /// Check whether the current symbol is valid or not for a given alphabet
        /// </summary>
        /// <param name="alphabet"></param>
        /// <param name="symbol">Symbol to be checked</param>
        /// <returns></returns>
        public static bool IsValidSymbol(IAlphabet alphabet, char symbol) {
            
            if (alphabet.GetValidSymbols().Contains((byte)symbol)) {
                return true;
            }

            return false;
        }


        /// <summary>
        /// Check wheter the current symbol is valid or not. This class would be used
        /// before the specific alphabet type is assigned.
        /// </summary>
        /// <param name="symbol">the symbol to be checked</param>
        /// <returns>True if a given symbol is a valid symbol either in DNA, RNA or Protein</returns>
        public static bool IsValidSymbol(char symbol)
        {

            if (DnaAlphabet.Instance.GetValidSymbols().Contains((byte)symbol))
            {
                return true;
            }
            else if (RnaAlphabet.Instance.GetValidSymbols().Contains((byte)symbol))
            {
                return true;
            }
            else if (ProteinAlphabet.Instance.GetValidSymbols().Contains((byte)symbol))
            {
                return true;
            }

            return false;
        }
    }
}
