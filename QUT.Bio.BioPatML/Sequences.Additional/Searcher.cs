﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QUT.Bio.BioPatML.Patterns;
using Bio;


/*****************| Queensland  University Of Technology |*******************
 *  Original Author          : Dr Stefan Maetschke
 *  Previous Work            : Samuel Toh 
 *  Adapted by               : Lalu Yazikri (Email : lalufahany.yazikri@student.qut.edu.au)
 *  Project supervisors      : Dr James Hogan
 *                             Mr Lawrence BuckingHam
 * 
 ***************************************************************************/

namespace QUT.Bio.BioPatML.Sequences.Additional
{
    /// <summary>
    /// This static class holds functions for search patterns 
    /// </summary>
    public static class Searcher
    {
        /// <summary>
        /// Searches for the pattern within the given sequence and creates a 
        /// <see cref="QUT.Bio.BioPatML.Sequences.List.FeatureList"> FeatureList </see> containning all matches.
        /// <para></para>
        /// Search finds only matches of the pattern which are within the specified range. 
        /// To find all matches in a sequence just call Searcher.SearchMatch(seq, 0,0, pattern) or
        /// SearchMatch(seq, 1,1, pattern) for example.
        /// <para></para>
        /// <example>
        /// <code>
        ///  Searcher.SearchMatch(seq, 0, seq.Count()-1, pattern);
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="startPos"> Start position for search (first position is zero).
        /// </param>
        /// <param name="endPos"> End position for search. If endPos less than or equals startPos the
        /// start position will be set to zero and the end position will be set to the 
        /// length of the sequence minus one. The end position can be greater than the length of 
        /// the sequence (to search across the genome boundary in circular genomes
        /// for instance).</param>
        /// <param name="pattern"> An object which implements the 
        /// <see cref="QUT.Bio.BioPatML.Patterns.IPattern">IPattern</see>
        /// interface. </param>
        /// <returns> Returns a <see cref="QUT.Bio.BioPatML.Sequences.List.FeatureList"> FeatureList </see>
        /// with all matches. 
        /// </returns>
        public static List<Match> SearchMatch(
            ISequence seq, 
            int startPos, 
            int endPos, 
            IPattern pattern)
        {
            List<Match> featureList = new List<Match>();
            Match match;
            StringBuilder sb = new StringBuilder();

            //add the sequence string
            if(endPos > (seq.Count-1))
            {
                for (int i = 0; i < (endPos - (seq.Count - 1)); i++ ) 
                {
                    sb.Append("-");
                }
                string additionalSeq = sb.ToString();
                seq = new Sequence(seq.Alphabet, seq.ToString() + additionalSeq);
            }

            //reset the index for searching
            if (endPos <= startPos)
            {
                startPos = 0;
                endPos = startPos + (int)seq.Count - 1;
            }

            while (startPos <= endPos)
            {
                
                match = pattern.Match(seq, startPos);

                if (match != null && match.End <= endPos)
                {
                    featureList.Add(match.Clone());
                }

                startPos += pattern.Increment;
                
            }

            return (featureList);
        }


        /// <summary>
        ///  Searches for the pattern with the highest similarity within the
        ///  given sequence. Search finds only matches of the pattern which are
        ///  within the specified range. 
        ///  <para></para>
        ///  To find all matches in a sequence just call Searcher.SearchBestMatch(seq, 0,0, pattern) or
        ///  Searcher.SearchBestMatch(seq, 1,1, pattern) for example.
        ///  <para></para>
        ///  <example>
        /// <code>
        ///  Searcher.SearchBestMatch(seq, 0, seq.Count()-1, pattern);
        /// </code>
        /// </example>
        /// </summary>
        /// <param name= "seq"></param>
        /// <param name="startPos"> Start position for search (first position is zero).
        /// The start position can not be negative.</param>
        /// <param name="endPos">End position for search. If endPos less than or equals startPos the
        /// start position will be set to zero and the end position will be set to the 
        /// length of the sequence minus one. 
        /// </param>
        /// <param name="pattern"> 
        /// An object which implements the <see cref="QUT.Bio.BioPatML.Patterns.IPattern">IPattern</see> interface,
        /// e.g. a sequence or a start weight matrix. 
        /// </param>
        /// <returns> 
        /// Returns a match object which describes the best match of
        /// the pattern.
        /// </returns>
            public static Match SearchBestMatch(ISequence seq, int startPos, int endPos, IPattern pattern)
            {
                Match maxMatch = new Match(null, null, 0, 0, Strand.Forward, -1.0);

                if (endPos <= startPos)
                {
                    startPos = 0;
                    endPos = startPos + (int)seq.Count - 1;
                }

                while (startPos <= endPos)
                {
                    Match match = pattern.Match(seq, startPos);
                    if (match != null
                        && match.Similarity > maxMatch.Similarity
                        && match.End <= endPos)
                        maxMatch.Set(match);

                    startPos += pattern.Increment;
                }

                if (maxMatch.Similarity < 0) //nothing found
                    return (null);

                maxMatch.BaseSequence = seq;
                return (maxMatch);
            }
        }
    }
