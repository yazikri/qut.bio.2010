﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Bio.IO;
using Bio;
using System.IO;
using QUT.Bio.BioPatML.Patterns;
using QUT.Bio.BioPatML.Readers;
using System.Xml.Linq;
using QUT.Bio.BioPatML.Sequences.Additional;

namespace PatternSearcher
{

    /// <summary>
    /// This simple application is developed to test the BioPatML library
    /// </summary>
    public partial class MainForm : Form
    {

        private List<Match> _matchList;

        private DataTable _dt_matches;

        private IList<ISequence> _sequences;

        private Definition _pattern;


        public MainForm()
        {
            InitializeComponent();
            InitializeDTMatches();
            BindGrid();
        }

        /// <summary>
        /// Bind Grid view with the data table
        /// </summary>
        private void BindGrid()
        {
            grid_show.DataSource = null;

            grid_show.DataSource = _dt_matches;
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitializeDTMatches()
        {
            _dt_matches = new DataTable();
            _dt_matches.Columns.Add("Start", typeof(long));
            _dt_matches.Columns.Add("Length", typeof(long));
            _dt_matches.Columns.Add("Letters", typeof(string) );
            _dt_matches.Columns.Add("Similarity", typeof(double));
        }

        private void FillDataTable()
        {
            ISequence seq = _sequences[0];
            _matchList = Searcher.SearchMatch(seq, 0, (int)seq.Count-1, _pattern.Pattern);

            _dt_matches.Clear();
            
            for (int i = 0; i < _matchList.Count; i++)
            {
                DataRow row = _dt_matches.NewRow();
                row[0] = _matchList[i].Start;
                row[1] = _matchList[i].Count;
                row[2] = _matchList[i].Letters();

                row[3] = _matchList[i].Similarity;
                _dt_matches.Rows.Add(row);
            }
        }

        private void btn_openGbk_Click(object sender, EventArgs e)
        {
            openGbk = new OpenFileDialog();

            openGbk.Title = "Select Sequence File";
            openGbk.Filter = "All Files|*.gbk";
            openGbk.RestoreDirectory = true;
            openGbk.CheckFileExists = true;

            if (openGbk.ShowDialog() == DialogResult.OK)
            {
                var filename = openGbk.FileName;
                var parser = SequenceParsers.FindParserByFileName(filename);
                this.textBox1.Text = filename;
                if (parser == null)
                {
                    MessageBox.Show("No parser available for file.",
                            "Unable to load file");
                    return;
                }

                try
                {
                    _sequences = parser.Parse().ToList();
                    parser.Close();
                    ShowSequence();
                }
                catch (Exception ex)
                {
                    _sequences = null;
                    MessageBox.Show(ex.Message, "Failed to parse " +
                                    Path.GetFileName(filename));
                }
            }
        }

        private void ShowSequence()
        {
            ISequence seq = _sequences.FirstOrDefault();
            txt_seqType.Text = seq.Alphabet.Name;
            txt_ID.Text = seq.ID;
            string seqString = seq.ToString();

            int multiple = 1;
            for (int i = 0; i < seqString.Length; i++ )
            {
                if (i / 50 == multiple) 
                {
                    seqString.Insert(i, "\r\n");
                    multiple++;
                }
            }

            txt_sequence.Text = seqString;
        }

        private void btn_openXML_Click(object sender, EventArgs e)
        {
            openXML = new OpenFileDialog();

            openXML.Title = "Select XML Pattern File";
            openXML.Filter = "All Files|*.xml";
            openXML.RestoreDirectory = true;
            openXML.CheckFileExists = true;

            if (openXML.ShowDialog() == DialogResult.OK)
            {
                var filename = openXML.FileName;

                this.textBox2.Text = filename;

                try
                {
                    XDocument doc = XDocument.Load(filename);
                    _pattern = DefinitionIO.Read(doc);
                    txtXML.Text = doc.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("cannot load the pattern into the definition :" + ex.ToString());

                }

            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            FillDataTable();
            BindGrid();
            txt_numMatches.Text = _dt_matches.Rows.Count.ToString();
        }

       
    }
}
