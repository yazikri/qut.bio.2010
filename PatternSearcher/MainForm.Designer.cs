﻿namespace PatternSearcher
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.openGbk = new System.Windows.Forms.OpenFileDialog();
            this.txt_numMatches = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.openXML = new System.Windows.Forms.OpenFileDialog();
            this.txtXML = new System.Windows.Forms.TextBox();
            this.btn_search = new System.Windows.Forms.Button();
            this.grid_show = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btn_openXML = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btn_openGbk = new System.Windows.Forms.Button();
            this.txt_sequence = new System.Windows.Forms.TextBox();
            this.txt_seqType = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_ID = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.grid_show)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(540, 471);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "No. Matches";
            // 
            // openGbk
            // 
            this.openGbk.FileName = "openGbk";
            this.openGbk.Filter = "Gbk Files|*.gbk";
            this.openGbk.RestoreDirectory = true;
            this.openGbk.Title = "Select Gen Bank (Gbk) File";
            // 
            // txt_numMatches
            // 
            this.txt_numMatches.Location = new System.Drawing.Point(638, 471);
            this.txt_numMatches.Name = "txt_numMatches";
            this.txt_numMatches.Size = new System.Drawing.Size(100, 20);
            this.txt_numMatches.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 351);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "View Pattern XML";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // openXML
            // 
            this.openXML.FileName = "openXML";
            this.openXML.Filter = "XML Files|*.xml";
            this.openXML.Title = "Select XML file";
            // 
            // txtXML
            // 
            this.txtXML.Location = new System.Drawing.Point(7, 367);
            this.txtXML.Multiline = true;
            this.txtXML.Name = "txtXML";
            this.txtXML.ReadOnly = true;
            this.txtXML.Size = new System.Drawing.Size(494, 124);
            this.txtXML.TabIndex = 20;
            // 
            // btn_search
            // 
            this.btn_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_search.Location = new System.Drawing.Point(271, 157);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.TabIndex = 19;
            this.btn_search.Text = "Search";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // grid_show
            // 
            this.grid_show.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_show.Location = new System.Drawing.Point(521, 48);
            this.grid_show.Name = "grid_show";
            this.grid_show.Size = new System.Drawing.Size(372, 417);
            this.grid_show.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Pattern";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(150, 111);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(202, 20);
            this.textBox2.TabIndex = 16;
            // 
            // btn_openXML
            // 
            this.btn_openXML.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_openXML.Location = new System.Drawing.Point(358, 108);
            this.btn_openXML.Name = "btn_openXML";
            this.btn_openXML.Size = new System.Drawing.Size(112, 23);
            this.btn_openXML.TabIndex = 15;
            this.btn_openXML.Text = "Open XML";
            this.btn_openXML.UseVisualStyleBackColor = true;
            this.btn_openXML.Click += new System.EventHandler(this.btn_openXML_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Sequence";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(150, 85);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(202, 20);
            this.textBox1.TabIndex = 13;
            // 
            // btn_openGbk
            // 
            this.btn_openGbk.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_openGbk.Location = new System.Drawing.Point(358, 82);
            this.btn_openGbk.Name = "btn_openGbk";
            this.btn_openGbk.Size = new System.Drawing.Size(112, 23);
            this.btn_openGbk.TabIndex = 12;
            this.btn_openGbk.Text = "Open GbK";
            this.btn_openGbk.UseVisualStyleBackColor = true;
            this.btn_openGbk.Click += new System.EventHandler(this.btn_openGbk_Click);
            // 
            // txt_sequence
            // 
            this.txt_sequence.Location = new System.Drawing.Point(7, 257);
            this.txt_sequence.Multiline = true;
            this.txt_sequence.Name = "txt_sequence";
            this.txt_sequence.ReadOnly = true;
            this.txt_sequence.Size = new System.Drawing.Size(494, 91);
            this.txt_sequence.TabIndex = 24;
            // 
            // txt_seqType
            // 
            this.txt_seqType.Enabled = false;
            this.txt_seqType.Location = new System.Drawing.Point(165, 235);
            this.txt_seqType.Name = "txt_seqType";
            this.txt_seqType.ReadOnly = true;
            this.txt_seqType.Size = new System.Drawing.Size(100, 20);
            this.txt_seqType.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 238);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Sequence";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(128, 238);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(282, 237);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "ID";
            // 
            // txt_ID
            // 
            this.txt_ID.Enabled = false;
            this.txt_ID.Location = new System.Drawing.Point(307, 234);
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.ReadOnly = true;
            this.txt_ID.Size = new System.Drawing.Size(100, 20);
            this.txt_ID.TabIndex = 29;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 503);
            this.Controls.Add(this.txt_ID);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt_seqType);
            this.Controls.Add(this.txt_sequence);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_numMatches);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtXML);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.grid_show);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.btn_openXML);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_openGbk);
            this.Name = "MainForm";
            this.Text = "Pattern Searcher";
            ((System.ComponentModel.ISupportInitialize)(this.grid_show)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.OpenFileDialog openGbk;
        private System.Windows.Forms.TextBox txt_numMatches;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openXML;
        private System.Windows.Forms.TextBox txtXML;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.DataGridView grid_show;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btn_openXML;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btn_openGbk;
        private System.Windows.Forms.TextBox txt_sequence;
        private System.Windows.Forms.TextBox txt_seqType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_ID;
    }
}

